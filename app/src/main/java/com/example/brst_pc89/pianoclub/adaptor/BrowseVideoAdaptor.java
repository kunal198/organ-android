package com.example.brst_pc89.pianoclub.adaptor;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.activities.BrowseVideoActivity;
import com.example.brst_pc89.pianoclub.classees.DateFormat;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.example.brst_pc89.pianoclub.classees.SongKey;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;


import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class BrowseVideoAdaptor extends BaseExpandableListAdapter {

    BrowseVideoActivity context;
    List<HashMap> responseList;
    LayoutInflater inflater;
    Dialog dialog;
    TextView logout_tv, logCancel_tv, log_tv;


    public BrowseVideoAdaptor(BrowseVideoActivity context, List<HashMap> responseList) {

        this.context = context;
        this.responseList = responseList;

        Log.d("sdds", responseList + "");
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }


    @Override
    public int getGroupCount() {
        return responseList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.custom_video_layout, null);
        TextView leftText_tv = (TextView) view.findViewById(R.id.leftText_tv);
        TextView centerText_tv = (TextView) view.findViewById(R.id.centerText_tv);

        HashMap hashMap = responseList.get(groupPosition);
        Object videoId = hashMap.get(MyConstant.VIDEO_ID);
        Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);


        leftText_tv.setText(videoTitle.toString());
        centerText_tv.setText("(" + videoArtist.toString() + ")");

        ImageView rowDown_iv = (ImageView) view.findViewById(R.id.rowDown_iv);

        if (isExpanded) {
            rowDown_iv.setImageResource(R.mipmap.ic_row_down);
            rowDown_iv.setRotation(360);
        }
        return view;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        View view = inflater.inflate(R.layout.customguest_expandablelayout, null);
        TextView videoLength_tv = (TextView) view.findViewById(R.id.videoLength_tv);
        TextView instuctor_tv = (TextView) view.findViewById(R.id.instuctor_tv);
        TextView songKey_tv = (TextView) view.findViewById(R.id.songKey_tv);
        TextView date_tv = (TextView) view.findViewById(R.id.date_tv);
        final ImageView watch_iv = (ImageView) view.findViewById(R.id.watch_iv);


        HashMap hashMap = responseList.get(groupPosition);
        final Object videoId = hashMap.get(MyConstant.VIDEO_ID);
        final Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        final Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);
        final Object publishDate = hashMap.get(MyConstant.PUBLISH_DATE);
        final Object videoRuntime = hashMap.get(MyConstant.VIDEO_RUNTIME);
        final Object songKey = hashMap.get(MyConstant.SONG_KEY);
        final Object instructorId = hashMap.get(MyConstant.INSTRUCTOR_ID);
        final Object mp4Name = hashMap.get(MyConstant.MP4_NAME);
        final Object youTube = hashMap.get(MyConstant.YOU_TUBE);


        final int songKey_val = Integer.parseInt(songKey.toString());
        final int instructor_val = Integer.parseInt(instructorId.toString());

        final String date = DateFormat.PARSE_DATE_FORMAT(publishDate.toString());

        videoLength_tv.setText(videoRuntime + " " + "min");

        songKey_tv.setText(SongKey.songKey_array[songKey_val]);
        date_tv.setText(date);
        instuctor_tv.setText(SongKey.instructor_array[instructor_val - 1]);


        final String videoPath = "https://s3.amazonaws.com/pchmp4/" + mp4Name;

        watch_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count = SharedPreference.getInstance().getCount(context, SharedPreference.VIDEO_COUNT);

                List<String> videoList = SharedPreference.getInstance().getVideoList(context, SharedPreference.VIDEO_LIST);

             /*   if(!videoList.contains(videoId))
                {
                    SharedPreference.getInstance().storeCount(context, SharedPreference.VIDEO_COUNT, count + 1);
                    count = SharedPreference.getInstance().getCount(context, SharedPreference.VIDEO_COUNT);
                 //   SharedPreference.getInstance().storeCount(context, SharedPreference.VIDEO_FLAG, 1);
                }

             else if(count>5 && videoList.contains(videoId))
                {
                    count=5;
                }*/

              /*  else
                {
                    SharedPreference.getInstance().storeCount(context, SharedPreference.VIDEO_FLAG, 0);
                }
*/

                int flag = SharedPreference.getInstance().getCount(context, SharedPreference.VIDEO_FLAG);
                SharedPreference.getInstance().storeData(context, SharedPreference.VIDEO_ID, videoId.toString());

                if (count < 5 || flag == 0) {

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                    intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
                    context.startActivityForResult(intent, 100);


                } else {


                    // INTERSITIAL();

                    REWARDED_VIDEO();
                }
            }
        });


        return view;
    }

    private void REWARDED_VIDEO() {

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        MobileAds.initialize(context, "ca-app-pub-6153266407346561~8544288338");

        final RewardedVideoAd rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);

        rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {


                SharedPreference.getInstance().storeCount(context, SharedPreference.VIDEO_FLAG, 1);

                Log.i("video", "Rewarded: onRewardedVideoAdLoaded");
                try {
                    if (rewardedVideoAd.isLoaded()) {
                        rewardedVideoAd.show();
                        progressDialog.dismiss();
                        SharedPreference.getInstance().storeData(context, SharedPreference.BOOLEAN, "true");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onRewardedVideoAdOpened() {

                Log.i("video", "Rewarded: onRewardedVideoAdOpened");

            }

            @Override
            public void onRewardedVideoStarted() {
                Log.i("video", "Rewarded: onRewardedVideoStarted");


            }

            @Override
            public void onRewardedVideoAdClosed() {

                Log.i("video", "Rewarded: onRewardedVideoAdClosed");

            }

            @Override
            public void onRewarded(RewardItem rewardItem) {

                SharedPreference.getInstance().storeCount(context, SharedPreference.VIDEO_FLAG, 0);

                Log.i("video", "onRewarded! currency: " + rewardItem.getType() + "  amount: " +
                        rewardItem.getAmount());

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {


                Log.i("video", "Rewarded: onRewardedVideoAdLeftApplication");

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {


                Log.i("video", "Rewarded: onRewardedVideoAdFailedToLoad: " + i);
                progressDialog.dismiss();
                NETWORK_DIALOG(context);
            }


        });

        rewardedVideoAd.loadAd(context.getString(R.string.rewarded_videoAd), new AdRequest.Builder().build());
    }


/*    private void INTERSITIAL() {


        final InterstitialAd interstitialAd;

        interstitialAd = new InterstitialAd(context);

        //set ad unit id

        interstitialAd.setAdUnitId(context.getString(R.string.interstitial_video));

        AdRequest adRequest = new AdRequest.Builder()

                *//*.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("C04B1BFFB0774708339BC273F8A43708")*//*
                .build();

        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {


            public void onAdLoaded() {


                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }

            }

            @Override
            public void onAdClosed() {
                // Toast.makeText(context, "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //  Toast.makeText(context, "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                //   List<String> videoList = SharedPreference.getInstance().getVideoList(context, SharedPreference.VIDEO_LIST);
                //   String videoId=SharedPreference.getInstance().getData(context, SharedPreference.VIDEO_ID);

              *//*  if(!videoList.contains(videoId))

                {
                    videoList.add(videoId);
                    SharedPreference.getInstance().storeArrayList(context,SharedPreference.VIDEO_LIST,videoList);
                }*//*
                SharedPreference.getInstance().storeCount(context, SharedPreference.VIDEO_FLAG, 0);
                // Toast.makeText(context, "Ad left application!", Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onAdOpened() {
                super.onAdOpened();
                SharedPreference.getInstance().storeCount(context, SharedPreference.VIDEO_FLAG, 1);
                // Toast.makeText(context, "Ad is opened!", Toast.LENGTH_SHORT).show();
            }


        });
    }*/


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);


    }

    private void NETWORK_DIALOG(Context context) {

        final Dialog dialognet = new Dialog(context);
        dialognet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialognet.setContentView(R.layout.dialog_filter);
        dialognet.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //   dialognet.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView content_tv = (TextView) dialognet.findViewById(R.id.content_tv);
        final TextView dialogOk_tv = (TextView) dialognet.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Network error.");

        dialogOk_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialognet.dismiss();
            }
        });
        dialognet.show();
    }
}
