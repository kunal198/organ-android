package com.example.brst_pc89.pianoclub.adaptor;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SongKey;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;
import com.example.brst_pc89.pianoclub.fragments.DownloadFragment;

import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 3/22/17.
 */
public class DownloadAdapter extends BaseExpandableListAdapter {

    Context context;
    String[] text, text2;
    LayoutInflater inflater;
    List<String> list;
    List<HashMap> databaseList;
    Dialog dialog;
    TextView logout_tv, logCancel_tv, log_tv;
    DownloadFragment downloadFragment;

    public DownloadAdapter(Context context, List<HashMap> databaseList, DownloadFragment downloadFragment) {

        this.context = context;
        this.downloadFragment=downloadFragment;

        this.databaseList = databaseList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        gET_LIST();
    }

    private void gET_LIST() {

        list = SqliteDataBase.SELECT_ID(context);
    }

    @Override
    public int getGroupCount() {
        return databaseList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.custom_favourite_layout, null);
        TextView leftText_tv = (TextView) view.findViewById(R.id.leftText_tv);
        TextView centerText_tv = (TextView) view.findViewById(R.id.centerText_tv);

        HashMap hashMap = databaseList.get(groupPosition);
        Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);

        leftText_tv.setText(videoTitle.toString());
        centerText_tv.setText("(" + videoArtist.toString() + ")");
        ImageView rowDown_iv = (ImageView) view.findViewById(R.id.rowDown_iv);
        if (isExpanded) {
            rowDown_iv.setImageResource(R.mipmap.ic_row_down);
            rowDown_iv.setRotation(360);
        }
        return view;
    }
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, final View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.custom_downloadexpandablelayout, null);
        ImageView expfavrt_iv = (ImageView) view.findViewById(R.id.expfavrt_iv);
        ImageView favrt_iv = (ImageView) view.findViewById(R.id.favrt_iv);
        ImageView watchOffline_iv = (ImageView) view.findViewById(R.id.watchOffline_iv);
        ImageView delete_iv = (ImageView) view.findViewById(R.id.delete_iv);

        TextView videoLength_tv = (TextView) view.findViewById(R.id.videoLength_tv);
        TextView instuctor_tv = (TextView) view.findViewById(R.id.instuctor_tv);
        TextView songKey_tv = (TextView) view.findViewById(R.id.songKey_tv);
        TextView date_tv = (TextView) view.findViewById(R.id.date_tv);


        HashMap hashMap = databaseList.get(groupPosition);
        final Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        final Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);
        final Object videoId = hashMap.get(MyConstant.VIDEO_ID);
        final Object videoPath = hashMap.get(MyConstant.VIDEO_PATH);
        Log.d("id", videoId + "");
        final Object publishDate = hashMap.get(MyConstant.PUBLISH_DATE);
        final Object videoRuntime = hashMap.get(MyConstant.VIDEO_RUNTIME);
        final Object songKey = hashMap.get(MyConstant.SONG_KEY);
        final Object instructorId = hashMap.get(MyConstant.INSTRUCTOR_ID);
        final Object mp4Name = hashMap.get(MyConstant.MP4_NAME);
        final Object youTube = hashMap.get(MyConstant.YOU_TUBE);

        if (list.contains(videoId)) {

            favrt_iv.setVisibility(View.VISIBLE);
            expfavrt_iv.setVisibility(View.GONE);
            Log.e("videoId while", videoId + "");
        }

        expfavrt_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SqliteDataBase.INSERT_DATA(context, videoId.toString(), videoTitle.toString(), videoArtist.toString(),
                        publishDate.toString(), videoRuntime.toString(), songKey.toString(),
                        instructorId.toString(), mp4Name.toString(), youTube.toString());

                Log.e("videoId", videoId + "------------" + videoTitle);

                gET_LIST();
                notifyDataSetChanged();


            }
        });

        favrt_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DELETE_DIALOG(context, videoId, false);


            }
        });


        videoLength_tv.setText(videoRuntime.toString() + " " + "min");
        instuctor_tv.setText(instructorId.toString());
        songKey_tv.setText(songKey.toString());
        date_tv.setText(publishDate.toString());

        watchOffline_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath.toString()));
                intent.setDataAndType(Uri.parse(videoPath.toString()), "video/mp4");
                context.startActivity(intent);
            }
        });
        delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DELETE_DIALOG(context, videoId, true);


            }
        });

        return view;
    }

    private void DELETE_DIALOG(final Context context, final Object videoId, final boolean b) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        logout_tv = (TextView) dialog.findViewById(R.id.logout_tv);
        logCancel_tv = (TextView) dialog.findViewById(R.id.logCancel_tv);
        log_tv = (TextView) dialog.findViewById(R.id.log_tv);
        if (b) {
            log_tv.setText("Confirm removal of this download?");
            logout_tv.setText("Ok");
            logCancel_tv.setText("Cancel");

        } else {
            log_tv.setText("Remove this favorite?");
        }


        logout_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!b) {

                    SqliteDataBase.DELETE_DATA(context, videoId.toString());
                    gET_LIST();
                    notifyDataSetChanged();
                } else {
                    SqliteDataBase.DELETE_VIDEO(context, videoId.toString());

                    if(downloadFragment instanceof DownloadFragment)
                    {
                        ((DownloadFragment)downloadFragment).Referesh_me();
                    }
                    CONFIRM_DIALOG(context);
                }


                dialog.dismiss();
            }
        });

        logCancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

    }
    private void CONFIRM_DIALOG(Context context) {

        final Dialog  dialogdel = new Dialog(context);
        dialogdel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogdel.setContentView(R.layout.dialog_filter);
        dialogdel.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView content_tv=(TextView)dialogdel.findViewById(R.id.content_tv);
        TextView dialogOk_tv=(TextView)dialogdel.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Successfully deleted.");
        dialogdel.show();

        dialogOk_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogdel.dismiss();
            }
        });
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}