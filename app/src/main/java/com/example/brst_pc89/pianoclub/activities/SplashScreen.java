package com.example.brst_pc89.pianoclub.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.MainActivity;
import com.example.brst_pc89.pianoclub.R;

import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.GetVersion;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;
import com.onesignal.OneSignal;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    private int SPLASH_TME_OUT = 1200,REQUEST_CODE=1000;
    String shared_data;
    int version_code;
    String version_name, versioncode;
    boolean value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //  OneSignal.startInit(this).init();
        //   startService(new Intent(getApplication(), OnClearFromRecentService.class));
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version_name = pInfo.versionName;
        version_code = pInfo.versionCode;

        //   GET_DATA_SERVER();



        shared_data = SharedPreference.getInstance().getData(getApplicationContext(), SharedPreference.API_RESPONSE);

        if (checkDrawOverlayPermission()) {

            PERMISSION();
        }




    }

    private void PERMISSION() {

        final NetworkInfo info = (NetworkInfo) ((ConnectivityManager) SplashScreen.this.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        //ask for authorisation
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 50);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d("xdat", shared_data);

                    //finish();



                    if (info == null) {

                        Log.d("sd","ddf");
                        Intent intent = new Intent(SplashScreen.this, ConnectActivity.class);
                         //Intent intent = new Intent(SplashScreen.this, BrowseLoginActivity.class);

                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        finish();
                    } else {
                        Log.d("sd","ddfvv");
                        pianoApiInterface pianoApiInterface = ApiClient.getClient().create(pianoApiInterface.class);
                        Call<GetVersion> getVersionCall = pianoApiInterface.version();
                        getVersionCall.enqueue(new Callback<GetVersion>() {
                            @Override
                            public void onResponse(Call<GetVersion> call, Response<GetVersion> response) {

                                Log.d("data", response.body().getAndriod_version_id());
                                String id = response.body().getId();
                                String version_id = response.body().getAndriod_version_id();
                                String maintance_mode=response.body().getMaintaince_mode();
                                Log.d("dsd",maintance_mode);

                                if(version_id.length()==1)
                                {
                                    version_id=version_id+".0";
                                }
                                if(version_name.length()<version_id.length())
                                {

                                    version_name=version_name+".0";
                                    Log.d("sd",version_name);
                                }
                                if(maintance_mode.equals("on"))
                                {


                                    Intent intent = new Intent(SplashScreen.this, MaintanceActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                    finish();

                                }
                                else  if (!version_name.equals(version_id)) {


                                    Intent intent = new Intent(SplashScreen.this, UpdateActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                    finish();

                                } else {

                                    if (shared_data.equals("premium: :asdf@asdf.com")) {

                                        Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                        finish();

                                    } else {

                                        Intent intent = new Intent(SplashScreen.this, BrowseLoginActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                        finish();
                                    }
                                }
                            }
                            @Override
                            public void onFailure(Call<GetVersion> call, Throwable t) {
                                //Intent intent = new Intent(SplashScreen.this, BrowseLoginActivity.class);
                                Intent intent = new Intent(SplashScreen.this, ConnectActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                finish();
                            }
                        });
                    }
                }
            }, SPLASH_TME_OUT);
        }

    }

    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(this)) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CODE) {

            if (android.os.Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(SplashScreen.this)) {


                    PERMISSION();
                } else {
                    finish();
                }
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 50:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Start your camera handling here
                    /*Toast.makeText(getApplicationContext(),"allow",Toast.LENGTH_LONG).show();*/
                    if (shared_data.equals("premium: :asdf@asdf.com")) {

                        Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

                    } else {

                        Intent intent = new Intent(SplashScreen.this, BrowseLoginActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                    }

                    finish();

                } else {
                    //  AppUtils.showUserMessage("You declined to allow the app to access your camera", this);
                    finish();
                /*	Toast.makeText(getApplicationContext(),"deny",Toast.LENGTH_LONG).show();*/
                }


        }
    }
}
