package com.example.brst_pc89.pianoclub.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.brst_pc89.pianoclub.Model.Category;
import com.example.brst_pc89.pianoclub.R;

import java.util.ArrayList;

/**
 * Created by brst-pc89 on 3/21/17.
 */
public class HomeAdaaptor extends BaseAdapter {
    Context context;
    ArrayList<Category> category;
    LayoutInflater inflater;

    public HomeAdaaptor(Context context,ArrayList<Category> category) {
        this.context=context;
        this.category=category;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return category.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=inflater.inflate(R.layout.custom_gridlayout,null);
        TextView gridetext_iv=(TextView) view.findViewById(R.id.gridtext_iv);
        gridetext_iv.setText(category.get(position).getName());

       /* Glide.with(context)
                .load(category.get(position).getName())
                .into(gridimage_iv);
*/

       // gridimage_iv.setImageResource(images[position]);
        return view;
    }
}
