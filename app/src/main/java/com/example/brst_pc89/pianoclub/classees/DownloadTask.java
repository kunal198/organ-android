package com.example.brst_pc89.pianoclub.classees;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.widget.BaseExpandableListAdapter;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.adaptor.AdaptorFavourite;
import com.example.brst_pc89.pianoclub.adaptor.BrowserAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.ChristianAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.ChristmasAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.GospelAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.OrganAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.PopularAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.SearchAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.TechinqueAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.ThemeAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.TheoryAdaptor;
import com.example.brst_pc89.pianoclub.fragments.FavouritesFragment;
import com.example.brst_pc89.pianoclub.fragments.PopularSongFragment;
import com.example.brst_pc89.pianoclub.fragments.TheoryFragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by brst-pc89 on 3/29/17.
 */
public class DownloadTask extends AsyncTask<String, Integer, String> {

     Context context;
     PowerManager.WakeLock mWakeLock;
    ProgressDialog mProgressDialog;
    String videoTitle,videoId,artist,date,videoRuntime,songKey_array,instructor_array,mp4Name,youTube;
    OutputStream output = null;
    File f;
    PopularAdaptor popularAdaptor;
    BaseExpandableListAdapter baseExpandableListAdapter;

    public DownloadTask(Context context, BaseExpandableListAdapter popularAdaptor, ProgressDialog mProgressDialog, String videoTitle, String videoId, String artist, String date, String videoRuntime, String songKey_array, String instructor_array, String mp4Name, String youTube) {
        this.context = context;
        this.baseExpandableListAdapter=popularAdaptor;
        this.mProgressDialog=mProgressDialog;
        this.videoTitle=videoTitle;
        this.videoId=videoId;
        this.artist=artist;
        this.date=date;
        this.videoRuntime=videoRuntime;
        this.songKey_array=songKey_array;
        this.instructor_array=instructor_array;
        this.mp4Name=mp4Name;
        this.youTube=youTube;
    }

    @Override
    protected String doInBackground(String... sUrl) {
        InputStream input = null;

        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            int fileLength = connection.getContentLength();
            Log.d("fdf",fileLength+"");


            input = connection.getInputStream();
//            File file = new File(Environment.getExternalStorageDirectory(), "VideoDownload");
//
             f=new File(Environment.getExternalStorageDirectory() + File.separator + videoTitle  + "Sample.mp4");

           output = new FileOutputStream(f);




            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                Log.d("count",count+"");
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire();
        mProgressDialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // if we get here, length is known, now set indeterminate to false
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgress(progress[0]);
        Log.d("progress",progress[0]+"");

       if(mProgressDialog.getProgress()==100)
       {
               String video_path=f.getAbsolutePath();
           Log.d("dvfxcfd",video_path);

           SqliteDataBase.INSERT_VIDEO(context,videoId,video_path,videoTitle,artist,date,videoRuntime,songKey_array,instructor_array,mp4Name,youTube);


       }
        else
       {
           String video_path=f.getAbsolutePath();
           Log.d("fd",video_path);
           Log.d("dsf","0");
       }
    }

    @Override
    protected void onPostExecute(String result) {

         //   Log.d("fgbfghresult",result);
      //  STORE_VIDEO_PATH(videoId)

        mWakeLock.release();
        mProgressDialog.dismiss();
        if (result != null) {
            mProgressDialog.show();
          //  Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            Log.d("downloaderror", result);
        }

        else {
            Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();


            if(baseExpandableListAdapter instanceof PopularAdaptor) {
                ((PopularAdaptor) baseExpandableListAdapter).refreshMe();
            }

            else if(baseExpandableListAdapter instanceof ChristianAdaptor) {
                ((ChristianAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof ChristmasAdaptor) {
                ((ChristmasAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof BrowserAdaptor) {
                ((BrowserAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof GospelAdaptor) {
                ((GospelAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof ThemeAdaptor) {
                ((ThemeAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof OrganAdaptor) {
                ((OrganAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof TechinqueAdaptor) {
                ((TechinqueAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof TechinqueAdaptor) {
                ((TheoryAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof AdaptorFavourite)
            {
                ((AdaptorFavourite) baseExpandableListAdapter).refreshMe();
            }

            else
            {
                ((SearchAdaptor) baseExpandableListAdapter).refreshMe();
            }
        }
    }
}