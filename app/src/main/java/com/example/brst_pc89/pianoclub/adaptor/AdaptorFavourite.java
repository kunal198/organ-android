package com.example.brst_pc89.pianoclub.adaptor;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.DownloadTask;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SongKey;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;
import com.example.brst_pc89.pianoclub.fragments.FavouritesFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 3/22/17.
 */
public class AdaptorFavourite extends BaseExpandableListAdapter {

    Context context;
    List<HashMap> list;
    LayoutInflater inflater;
    Dialog dialog;
    TextView logout_tv, logCancel_tv, log_tv;
    List<HashMap> videoList;
    ;

    public AdaptorFavourite(Context context, List<HashMap> list) {

        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Log.d("list",list+"");
        GET_DATA_LIST();

    }

    private void GET_DATA_LIST() {

        videoList=SqliteDataBase.SELECT_DOWNLOAD_VIDEO(context);
       // Log.d("databaselist",databaseList+"");


    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.custom_favourite_layout, null);
        TextView leftText_tv = (TextView) view.findViewById(R.id.leftText_tv);
        TextView centerText_tv = (TextView) view.findViewById(R.id.centerText_tv);

        HashMap hashMap = list.get(groupPosition);


        Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);

        leftText_tv.setText(videoTitle.toString());

        centerText_tv.setText("(" + videoArtist.toString() + ")");

        ImageView rowDown_iv = (ImageView) view.findViewById(R.id.rowDown_iv);

        if (isExpanded) {
            rowDown_iv.setImageResource(R.mipmap.ic_row_down);
            rowDown_iv.setRotation(360);
        }
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        View view = inflater.inflate(R.layout.custom_expandablelayout, null);
        ImageView expfavrt_iv = (ImageView) view.findViewById(R.id.expfavrt_iv);
        ImageView favrt_iv = (ImageView) view.findViewById(R.id.favrt_iv);
        ImageView watch_iv = (ImageView) view.findViewById(R.id.watch_iv);
        final ImageView watchOffline_iv = (ImageView) view.findViewById(R.id.watchOffline_iv);
      //  final ImageView downloaded_iv = (ImageView) view.findViewById(R.id.downloaded_iv);
        final ImageView download_iv = (ImageView) view.findViewById(R.id.download_iv);
        final ImageView delete_iv = (ImageView) view.findViewById(R.id.delete_iv);



        expfavrt_iv.setVisibility(View.GONE);
        favrt_iv.setVisibility(View.VISIBLE);

        final TextView download_tv = (TextView) view.findViewById(R.id.download_tv);
        TextView videoLength_tv = (TextView) view.findViewById(R.id.videoLength_tv);
        TextView instuctor_tv = (TextView) view.findViewById(R.id.instuctor_tv);
        TextView songKey_tv = (TextView) view.findViewById(R.id.songKey_tv);
        TextView date_tv = (TextView) view.findViewById(R.id.date_tv);

        HashMap hashMap = list.get(groupPosition);
        final Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        final Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);
        final Object videoId = hashMap.get(MyConstant.VIDEO_ID);
        Log.d("id", videoId + "");
        final Object publishDate = hashMap.get(MyConstant.PUBLISH_DATE);
        final Object videoRuntime = hashMap.get(MyConstant.VIDEO_RUNTIME);
        final Object songKey = hashMap.get(MyConstant.SONG_KEY);
        final Object instructorId = hashMap.get(MyConstant.INSTRUCTOR_ID);
        final Object mp4Name = hashMap.get(MyConstant.MP4_NAME);
        final Object youTube = hashMap.get(MyConstant.YOU_TUBE);


        videoLength_tv.setText(videoRuntime + " " + "min");

        songKey_tv.setText(songKey.toString());
        date_tv.setText(publishDate.toString());
        instuctor_tv.setText(instructorId.toString());

        favrt_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DELETE_DIALOG(context, videoId,false);

            }
        });

        for (int i = 0; i < videoList.size(); i++) {
            if (videoList.get(i).get(MyConstant.VIDEO_ID).equals(videoId)) {
                //  videoPath=videoList.get(i).get(MyConstant.VIDEO_PATH).toString();
                Log.d("dfd", "dsfs");
                download_iv.setVisibility(View.GONE);
                watchOffline_iv.setVisibility(View.VISIBLE);
                watch_iv.setVisibility(View.GONE);
                download_tv.setVisibility(View.VISIBLE);
                delete_iv.setVisibility(View.VISIBLE);
                break;
            }
        }

        watchOffline_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Path = GET_PATH(videoId.toString());
                Log.d("fd", Path);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Path));
                intent.setDataAndType(Uri.parse(Path), "video/mp4");
                context.startActivity(intent);
            }
        });


        final String videoPath = "https://s3.amazonaws.com/pchmp4/" + mp4Name;
        watch_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
                context.startActivity(intent);


            }
        });

        download_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProgressDialog mProgressDialog;
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage(videoTitle.toString());
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);


                final DownloadTask downloadTask = new DownloadTask(context,AdaptorFavourite.this, mProgressDialog, videoTitle.toString(), videoId.toString(),videoArtist.toString(),
                        publishDate.toString(), videoRuntime.toString(), songKey.toString(),
                        instructorId.toString(), mp4Name.toString(), youTube.toString());

                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        downloadTask.cancel(true);
                    }
                });

                downloadTask.execute("https://s3.amazonaws.com/pchmp4/" + mp4Name);

                mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        downloadTask.cancel(true);
                    }
                });

            }
        });

        delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DELETE_DIALOG(context, videoId, true);


            }
        });

        return view;


    }

    private String GET_PATH(String videoId) {

        String videoPath = "";
        for (int i = 0; i < videoList.size(); i++) {
            if (videoList.get(i).get(MyConstant.VIDEO_ID).equals(videoId)) {
                videoPath = videoList.get(i).get(MyConstant.VIDEO_PATH).toString();
                break;
            }
        }
        return videoPath;
    }

    public void refreshMe()
    {
        GET_DATA_LIST();
        notifyDataSetChanged();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }


    private void DELETE_DIALOG(final Context context, final Object videoId, final boolean b) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        logout_tv = (TextView) dialog.findViewById(R.id.logout_tv);
        logCancel_tv = (TextView) dialog.findViewById(R.id.logCancel_tv);
        log_tv = (TextView) dialog.findViewById(R.id.log_tv);
        if (b) {
            log_tv.setText("Confirm removal of this download?");
            logout_tv.setText("Ok");
            logCancel_tv.setText("Cancel");

        } else {
            log_tv.setText("Remove this favorite?");
        }


        logout_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!b) {

                    SqliteDataBase.DELETE_DATA(context, videoId.toString());
                    SqliteDataBase.SELECT_DATA(context);
                    notifyDataSetChanged();
                }
                else
                {
                    SqliteDataBase.DELETE_VIDEO(context, videoId.toString());
                    GET_DATA_LIST();
                    notifyDataSetChanged();

                    CONFIRM_DIALOG(context);

                }

                FavouritesFragment.headerBelow_tv.setText(list.size() + " " + "videos, sorted by publish date");
                dialog.dismiss();

            }
        });

        logCancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

    }

    private void CONFIRM_DIALOG(Context context) {

        final Dialog  dialogdel = new Dialog(this.context);
        dialogdel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogdel.setContentView(R.layout.dialog_filter);
        dialogdel.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView content_tv=(TextView)dialogdel.findViewById(R.id.content_tv);
        TextView dialogOk_tv=(TextView)dialogdel.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Successfully deleted.");
        dialogdel.show();

        dialogOk_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogdel.dismiss();
            }
        });
    }

}
