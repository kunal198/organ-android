package com.example.brst_pc89.pianoclub.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;

/**
 * Created by brst-pc89 on 4/20/17.
 */
public class CustomSpinnerAdaptor extends ArrayAdapter<Object> {

    Context context;
    String[] objects;
    String firstElement;
    boolean isFirstTime;

    public CustomSpinnerAdaptor(Context theContext, int theLayoutResId, String[] dropdown_list) {
        super(theContext, theLayoutResId, dropdown_list);
    }

    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }
}


