package com.example.brst_pc89.pianoclub.fragments;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.adaptor.AdaptorFavourite;
import com.example.brst_pc89.pianoclub.adaptor.ChristmasAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.DownloadAdapter;
import com.example.brst_pc89.pianoclub.adaptor.GospelAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.ThemeAdaptor;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


public class DownloadFragment extends Fragment implements View.OnClickListener {

    ImageView popUpdismiss_iv, popUp_iv;


    ExpandableListView downloadFragment_lv;
    List<HashMap> databaseList;
    List<String> list;
    DownloadFragment downloadFragment;
    TextView header_below;
    public static PopupWindow popupWindow;
    boolean showingFirst = false;

    public DownloadFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_download, container, false);

        downloadFragment = this;
        GET_LAYOUT_ID(view);


        SELECT_DATA();


        popUp_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopup(popUp_iv);
            }
        });


        return view;
    }

    public void SELECT_DATA() {


        databaseList = SqliteDataBase.SELECT_DOWNLOAD_VIDEO(getContext());

        Log.d("list", list + "");
        Log.d("data", databaseList + "");
        header_below.setText(list.size() + " " + "videos, sorted by publish date");

        DownloadAdapter downloadAdapter = new DownloadAdapter(getContext(), databaseList, downloadFragment);
        downloadFragment_lv.setAdapter(downloadAdapter);


    }

    private void GET_LAYOUT_ID(View view) {

        popUp_iv = (ImageView) view.findViewById(R.id.popUp_iv);
        header_below = (TextView) view.findViewById(R.id.header_below);
        popUpdismiss_iv = (ImageView) view.findViewById(R.id.popUpdismiss_iv);
        downloadFragment_lv = (ExpandableListView) view.findViewById(R.id.downloadFragment_lv);
        list = new ArrayList<String>();
        databaseList = new ArrayList<HashMap>();

    }


    private void showPopup(ImageView popUp) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_popup, null);
        TextView title_tv = (TextView) popupView.findViewById(R.id.title_tv);
        TextView artist_tv = (TextView) popupView.findViewById(R.id.artist_tv);
        TextView date_tv = (TextView) popupView.findViewById(R.id.date_tv);

        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //TODO do sth here on dismiss
                popupWindow.dismiss();
            }
        });

        popupWindow.showAsDropDown(popUp, 0, -40);

        popUp_iv.setVisibility(View.GONE);
        popUpdismiss_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                popUp_iv.setVisibility(View.VISIBLE);
                popUpdismiss_iv.setVisibility(View.GONE);

            }
        });

        title_tv.setOnClickListener(this);
        artist_tv.setOnClickListener(this);
        date_tv.setOnClickListener(this);
    }

    public void Referesh_me() {

        SELECT_DATA();
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.title_tv:

                if (showingFirst == false) {
                    SORT_LIST("title");
                    showingFirst = true;
                } else {

                    DES_LIST("title");

                    showingFirst = false;
                }
                break;

            case R.id.artist_tv:


                if (showingFirst == false) {
                    SORT_LIST("artist");
                    showingFirst = true;
                } else {

                    DES_LIST("artist");

                    showingFirst = false;
                }

                break;

            case R.id.date_tv:

                if (showingFirst == false) {
                    SORT_LIST("dateformat");
                    showingFirst = true;
                } else {

                    DES_LIST("dateformat");

                    showingFirst = false;
                }


                break;



        }
    }

    private void DES_LIST(final String string) {

        Collections.sort(databaseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return secondValue.compareToIgnoreCase(firstValue);
            }
        });

        DownloadAdapter downloadAdapter = new DownloadAdapter(getContext(), databaseList, downloadFragment);
        downloadFragment_lv.setAdapter(downloadAdapter);

        if (string.equals("title")) {
            header_below.setText(databaseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            header_below.setText(databaseList.size() + " " + "videos, sorted by artist");
        } else {
            header_below.setText(databaseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }

    public void SORT_LIST(final String string) {

        Collections.sort(databaseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return firstValue.compareToIgnoreCase(secondValue);
            }
        });

        DownloadAdapter downloadAdapter = new DownloadAdapter(getContext(), databaseList, downloadFragment);
        downloadFragment_lv.setAdapter(downloadAdapter);

        if (string.equals("title")) {
            header_below.setText(databaseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            header_below.setText(databaseList.size() + " " + "videos, sorted by artist");
        } else {
            header_below.setText(databaseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }
}
