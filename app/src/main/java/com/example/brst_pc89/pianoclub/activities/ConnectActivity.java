package com.example.brst_pc89.pianoclub.activities;

import android.animation.IntEvaluator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;

import java.util.HashMap;
import java.util.List;

public class ConnectActivity extends AppCompatActivity {

    ImageView offline_iv;
    TextView internet_iv;
    List<HashMap> databaseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        offline_iv=(ImageView)findViewById(R.id.offline_iv);
        internet_iv=(TextView)findViewById(R.id.internet_iv);

        databaseList = SqliteDataBase.SELECT_DOWNLOAD_VIDEO(getBaseContext());
        Log.d("czxc",databaseList+"");

        if(databaseList.size()==0)
        {
            offline_iv.setVisibility(View.GONE);
            internet_iv.setVisibility(View.VISIBLE);
        }

        offline_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ConnectActivity.this, DownLoadActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                finish();
            }
        });

        internet_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ConnectActivity.this, SplashScreen.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                finish();
            }
        });

    }
}
