package com.example.brst_pc89.pianoclub.classees;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class DateFormat {

    public static final String PARSE_DATE_FORMAT(String publishDate) {

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "MM/dd/yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String dateformat = null;

        try {
            date = inputFormat.parse(publishDate);
            dateformat = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateformat;

    }
}
