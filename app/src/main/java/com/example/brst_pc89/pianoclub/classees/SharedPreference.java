package com.example.brst_pc89.pianoclub.classees;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class SharedPreference {

    public final String PREFERENCE_NAME = "pianoClub_pref";
    public static final String API_RESPONSE = "pianoClub_pref";
    public static final String API_UserName = "uSerName";
    public static final String API_UserNameRequest = "uSerNameRequest";
    public static final String API_PassWord = "pAssWord";
    public static final String KEY_LIST = "KeyList";
    public static final String KEY_SIZE = "keysize";
    public static final String GENRE_SIZE = "genresize";
    public static final String VIDEO_SIZE = "videoSize";
    public static final String INS_SIZE = "inssize";
    public static final String GENRE_LIST = "genreList";
    public static final String GENRE_LISTNAME = "genreListName";

    public static final String INSTRUCTOR_LIST = "instructorList";
    public static final String VIDEO_LIST = "videoList";
    public static final String VIDEO_COUNT = "video_count";
    public static final String VIDEO_FLAG= "video_flag";
    public static final String VIDEO_ID= "video_id";
    public static final String BOOLEAN= "bool_val";

    private static SharedPreference instance = null;

    public static SharedPreference getInstance() {
        if (instance == null) {
            instance = new SharedPreference();
        }
        return instance;
    }



    public SharedPreferences getPreference(Context context) {
        return context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }


    public void storeData(Context context, String key, String value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(key, value);
        editor.apply();

        Log.e("storedata", "storedata");


    }

    public void storeCount(Context context,String key,int val)
    {
        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putInt(key, val);
        editor.apply();

    }

    public void storeArrayList(Context context, String key, List<String> list) {

        SharedPreferences.Editor editor = getPreference(context).edit();

        for (int i = 0; i < list.size(); i++) {
            editor.putString(key + i, list.get(i));
        }
        if(key.equals(SharedPreference.KEY_LIST)) {
            editor.putInt(KEY_SIZE, list.size());
        }
        if(key.equals(SharedPreference.GENRE_LIST)) {
            editor.putInt(GENRE_SIZE, list.size());
        }
        if(key.equals(SharedPreference.INSTRUCTOR_LIST)) {
            editor.putInt(INS_SIZE, list.size());
        }

        if(key.equals(SharedPreference.VIDEO_LIST))
        {
            editor.putInt(VIDEO_SIZE,list.size());
        }
        editor.apply();

        Log.e("storedata", "storedata");
        Log.e("storedata", list.toString());


    }

    public ArrayList<String> geKeyList(Context context, String key) {

        ArrayList<String> keyList = new ArrayList<String>();
        int size = getPreference(context).getInt(KEY_SIZE, 0);

        for (int j = 0; j < size; j++) {
            if(!getPreference(context).getString(key + j, "").isEmpty())
            keyList.add(getPreference(context).getString(key + j,null));
        }
        return keyList;
    }
    public ArrayList<String> geGenreList(Context context, String key) {

        ArrayList<String> genreList = new ArrayList<String>();
        int size = getPreference(context).getInt(GENRE_SIZE, 0);

        for (int j = 0; j < size; j++) {
            if(!getPreference(context).getString(key + j, "").isEmpty())
            genreList.add(getPreference(context).getString(key + j, ""));
        }
        return genreList;
    }

    public ArrayList<String> geInstructorList(Context context, String key) {

        ArrayList<String> insList = new ArrayList<String>();
        int size = getPreference(context).getInt(INS_SIZE, 0);

        for (int j = 0; j < size; j++)
        {
            if(!getPreference(context).getString(key + j, "").isEmpty())
            insList.add(getPreference(context).getString(key + j, ""));
        }
        return insList;
    }

    public ArrayList<String> getVideoList(Context context, String key) {

        ArrayList<String> videoList = new ArrayList<String>();
        int size = getPreference(context).getInt(VIDEO_SIZE, 0);

        for (int j = 0; j < size; j++) {
            if(!getPreference(context).getString(key + j, "").isEmpty())
                videoList.add(getPreference(context).getString(key + j,null));
        }
        return videoList;
    }

    public void clearData(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (preferences != null)
            preferences.edit().remove(key).apply();

    }


    public void clearDataList(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        ArrayList<String> keyList = new ArrayList<String>();
        int size = 0;
        if(key.equals(KEY_LIST)) {
             size = getPreference(context).getInt(KEY_SIZE, 0);
        }
         if(key.equals(GENRE_LIST)) {
            size = getPreference(context).getInt(GENRE_SIZE, 0);
        }
        if(key.equals(INSTRUCTOR_LIST))
        {
            size = getPreference(context).getInt(INS_SIZE, 0);
        }
        if (preferences != null)

        {
            for (int i = 0; i < size;i++) {
                preferences.edit().remove(key + i).apply();
              //  keyList.remove(key+i);
            }
        }

    }

    public String getData(Context context, String key) {

        Log.e("getdata", "getdata");

        return getPreference(context).getString(key, "null");
    }
  public int getCount(Context context, String key)
  {
      return getPreference(context).getInt(key,0);
  }

}


