package com.example.brst_pc89.pianoclub.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;

public class MaintanceActivity extends AppCompatActivity implements View.OnClickListener{

    TextView checkLater_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintancemode);

        checkLater_tv=(TextView)findViewById(R.id.checkLater_tv);
        checkLater_tv.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.checkLater_tv:

                finish();
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);

                break;
        }
    }
}
