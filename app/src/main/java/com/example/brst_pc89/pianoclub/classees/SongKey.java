package com.example.brst_pc89.pianoclub.classees;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class SongKey {

    public static final String songKey_array[] = {"n/a","C", "C#/Db", "D", "D#/Eb", "E", "F", "F#/Gb", "G", "G#/Ab", "A", "A#/Bb", "B"};

    public static final String instructor_array[] = {"Tj Hanes", "Carlton Whitfield",
            "Darrell Cook", "Shaun Martin",
            "Rick Barclay", "David Papanagis", "",
            "Peter Thompson", "Stephano Buchanan",
            "Anthony Brice", "Anthony Brice"};


}
