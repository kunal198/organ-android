package com.example.brst_pc89.pianoclub.classees;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class MyConstant {

    public static  final String VIDEO_ID="VideoID";
    public static  final String VIDEO_TITLE="VideoTitle";
    public static  final String VIDEO_ARTIST="VideoArtist";
    public static  final String PUBLISH_DATE="PublishDate";
    public static  final String VIDEO_RUNTIME="VideoRuntime";
    public static  final String SONG_KEY="SongKey";
    public static  final String INSTRUCTOR_ID="InstructorID";
    public static  final String MP4_NAME="MP4Name";
    public static  final String YOU_TUBE="YouTubePreviewID";
    public static  final String SAVEDATA_DATABASE="ApiDatabase";
    public static  final String VIDEO_PATH="VideoPath";
}
