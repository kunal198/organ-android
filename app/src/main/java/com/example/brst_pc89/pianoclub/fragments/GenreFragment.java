package com.example.brst_pc89.pianoclub.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;

import com.example.brst_pc89.pianoclub.Model.Category;
import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.adaptor.CustomGenre;
import com.example.brst_pc89.pianoclub.adaptor.GenerAdaptor;
import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GenreFragment extends Fragment implements View.OnClickListener{

     ImageView backfrag_iv,genrePop_iv,genreGos_iv,genreChis_iv,genreTheo_iv,
             genreThem_iv,genreChris_iv,genreTec_iv;

    ArrayList<String> genreList;
    ListView listGenre;
    ProgressDialog progressDialog;
    ArrayList<Category> arrayListCategory=new ArrayList<>();
    CustomGenre genre;
    ArrayList<String> arrayListName=new ArrayList<>();

    public GenreFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_genre, container, false);
        backfrag_iv=(ImageView)view.findViewById(R.id.backfrag_iv);
        genrePop_iv=(ImageView)view.findViewById(R.id.genrePop_iv);
        genreGos_iv=(ImageView)view.findViewById(R.id.genreGos_iv);
        genreChis_iv=(ImageView)view.findViewById(R.id.genreChis_iv);
        genreTheo_iv=(ImageView)view.findViewById(R.id.genreTheo_iv);
        genreThem_iv=(ImageView)view.findViewById(R.id.genreThem_iv);
        genreChris_iv=(ImageView)view.findViewById(R.id.genreChris_iv);
        genreTec_iv=(ImageView)view.findViewById(R.id.genreTec_iv);
        listGenre=(ListView)view.findViewById(R.id.listGenre) ;
        genreList=new ArrayList<String>();

        genreList= SharedPreference.getInstance().geGenreList(getContext(),SharedPreference.GENRE_LIST);
        Log.d("jjj",genreList+"");

        GET_SELECTED_BTN();
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        genre=new CustomGenre(getActivity(),arrayListCategory,genreList);
        listGenre.setAdapter(genre);

        GET_DATA_SERVER();

        backfrag_iv.setOnClickListener(this);
        genrePop_iv.setOnClickListener(this);
        genreGos_iv.setOnClickListener(this);
        genreChis_iv.setOnClickListener(this);
        genreTheo_iv.setOnClickListener(this);
        genreThem_iv.setOnClickListener(this);
        genreChris_iv.setOnClickListener(this);
        genreTec_iv.setOnClickListener(this);


        return view;
    }

    private void GET_SELECTED_BTN() {

        if(genreList.contains(String.valueOf(1)))
            genrePop_iv.setImageLevel(20);

        if(genreList.contains(String.valueOf(6)))
            genreGos_iv.setImageLevel(20);

        if(genreList.contains(String.valueOf(11)))
            genreChis_iv.setImageLevel(20);

        if(genreList.contains(String.valueOf(4)))
            genreTheo_iv.setImageLevel(20);

        if(genreList.contains(String.valueOf(8)))
            genreThem_iv.setImageLevel(20);

        if(genreList.contains(String.valueOf(7)))
            genreChris_iv.setImageLevel(20);

        if(genreList.contains(String.valueOf(5)))
            genreTec_iv.setImageLevel(20);


    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.backfrag_iv:

             //   SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;

            case R.id.genrePop_iv:

                if(genrePop_iv.getDrawable().getLevel()==20)
                {

                    genrePop_iv.setImageLevel(10);

                    if(genreList.contains(String.valueOf(1))) {

                        genreList.remove(String.valueOf(1));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                    }

                }
                else
                {
                    genrePop_iv.setImageLevel(20);
                    genreList.add(String.valueOf(1));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                }
                break;

            case R.id.genreGos_iv:

                if(genreGos_iv.getDrawable().getLevel()==20)
                {

                    genreGos_iv.setImageLevel(10);

                    if(genreList.contains(String.valueOf(6))) {

                        genreList.remove(String.valueOf(6));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                    }

                }
                else
                {
                    genreGos_iv.setImageLevel(20);
                    genreList.add(String.valueOf(6));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);

                }
                break;

            case R.id.genreChis_iv:

                if(genreChis_iv.getDrawable().getLevel()==20)
                {

                    genreChis_iv.setImageLevel(10);

                    if(genreList.contains(String.valueOf(11))) {

                        genreList.remove(String.valueOf(11));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                    }

                }
                else
                {
                    genreChis_iv.setImageLevel(20);
                    genreList.add(String.valueOf(11));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);

                }
                break;

            case R.id.genreTheo_iv:

                if(genreTheo_iv.getDrawable().getLevel()==20)
                {

                    genreTheo_iv.setImageLevel(10);

                    if(genreList.contains(String.valueOf(4))) {

                        genreList.remove(String.valueOf(4));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                    }

                }
                else
                {
                    genreTheo_iv.setImageLevel(20);
                    genreList.add(String.valueOf(4));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);

                }
                break;

            case R.id.genreThem_iv:

                if(genreThem_iv.getDrawable().getLevel()==20)
                {

                    genreThem_iv.setImageLevel(10);

                    if(genreList.contains(String.valueOf(8))) {

                        genreList.remove(String.valueOf(8));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                    }

                }
                else
                {
                    genreThem_iv.setImageLevel(20);
                    genreList.add(String.valueOf(8));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);

                }
                break;

            case R.id.genreChris_iv:

                if(genreChris_iv.getDrawable().getLevel()==20)
                {

                    genreChris_iv.setImageLevel(10);

                    if(genreList.contains(String.valueOf(7))) {

                        genreList.remove(String.valueOf(7));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                    }

                }
                else
                {
                    genreChris_iv.setImageLevel(20);
                    genreList.add(String.valueOf(7));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);

                }
                break;

            case R.id.genreTec_iv:

                if(genreTec_iv.getDrawable().getLevel()==20)
                {

                    genreTec_iv.setImageLevel(10);

                    if(genreList.contains(String.valueOf(5))) {

                        genreList.remove(String.valueOf(5));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);
                    }

                }
                else
                {
                    genreTec_iv.setImageLevel(20);
                    genreList.add(String.valueOf(5));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.GENRE_LIST, genreList);

                }
                break;

        }
    }

    private void GET_DATA_SERVER() {

        progressDialog.show();

        pianoApiInterface apiInterface = ApiClient.getClient().create(pianoApiInterface.class);
        Call<List<HashMap>> category = apiInterface.category();
        category.enqueue(new Callback<List<HashMap>>() {
            @Override
            public void onResponse(Call<List<HashMap>> call, Response<List<HashMap>> response) {

                progressDialog.dismiss();
                //dropdown = response.body();
                Category category;
                arrayListCategory.clear();
                /*category=new Category();
                category.setDescription("Desc");
                category.setName("Browse All Videos");
                category.setId("0");*/
                //arrayListCategory.add(category);

                for (int i = 0; i < response.body().size(); i++) {

                    category = new Category();
                    category.setName(response.body().get(i).get("NAME").toString());
                    category.setId(response.body().get(i).get("ID").toString());
                    category.setDescription(response.body().get(i).get("DESCRIPTION").toString());
                    arrayListCategory.add(category);
                }
                genre.notifyDataSetChanged();
                Log.e("sizesss", arrayListCategory.size() + "");


            }

            @Override
            public void onFailure(Call<List<HashMap>> call, Throwable t) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        progressDialog.dismiss();

                    }
                }, 2000);
            }
        });

    }
}
