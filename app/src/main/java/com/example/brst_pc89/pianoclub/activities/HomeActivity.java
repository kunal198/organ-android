package com.example.brst_pc89.pianoclub.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.ExampleNotificationOpenHandler;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.example.brst_pc89.pianoclub.fragments.DownloadFragment;
import com.example.brst_pc89.pianoclub.fragments.FavouritesFragment;
import com.example.brst_pc89.pianoclub.fragments.HomeFragment;
import com.example.brst_pc89.pianoclub.fragments.KeyFragment;
import com.example.brst_pc89.pianoclub.fragments.PopularSongFragment;
import com.example.brst_pc89.pianoclub.fragments.SearchFragment;
import com.example.brst_pc89.pianoclub.fragments.SearchResultFragment;
import com.onesignal.OneSignal;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener {


    TabLayout tabs_tl;

    FrameLayout container_fl;

    Dialog dialog;

    TextView logout_tv, logCancel_tv;

    HomeFragment homeFragment;
    SearchFragment searchFragment;
    FavouritesFragment favouritesFragment;
    DownloadFragment downloadFragment;

    boolean doubleBackToExitPressedOnce = true, count = false;

    int selectedImage[] = {R.mipmap.ic_home_selected,
            R.mipmap.ic_search_selected,
            R.mipmap.ic_favorite_sel_,
            R.mipmap.ic_down_selected,
            R.mipmap.ic_logout_selected};

    int unselectedImage[] = {R.mipmap.ic_home_unselected,
            R.mipmap.ic_search_unselect,
            R.mipmap.ic_favorite_unselect,
            R.mipmap.ic_download_unselect,
            R.mipmap.ic_logout_unselect

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new ExampleNotificationOpenHandler())
                .init();


        getAllWidgets();

        setUpTabLayout();

        bindWidgetsWithAnEvent();

        setCurrentTabFragment(0);

//        try {
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone(getApplication(), notification);
//            r.play();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    private void bindWidgetsWithAnEvent() {

        tabs_tl.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                setCurrentTabFragment(tab.getPosition());

                tab.setIcon(selectedImage[tab.getPosition()]);
                /*for (int i = 0; i < tabs_tl.getTabCount(); i++) {
                    if (i == tab.getPosition()) {
                        tabs_tl.getTabAt(i).getCustomView()
                                .setBackgroundColor(Color.parseColor("#198C19"));
                    } else {
                        tabs_tl.getTabAt(i).getCustomView()
                                .setBackgroundColor(Color.parseColor("#f4f4f4"));
                    }
                }*/
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                tab.setIcon(unselectedImage[tab.getPosition()]);

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                Log.d("dfsf", "sdf");
                setCurrentTabFragment(tab.getPosition());
                tab.setIcon(selectedImage[tab.getPosition()]);


            }
        });

    }

    private void setCurrentTabFragment(int position) {

        switch (position) {
            case 0:

                replaceFragment(homeFragment);


                break;
            case 1:

                replaceFragment(searchFragment);

                break;
            case 2:
                replaceFragment(favouritesFragment);
                break;
            case 3:
                replaceFragment(downloadFragment);
                break;
            case 4:
                // replaceFragment(homeFragment);
                showDialog(this);

                break;
        }
    }

    private void showDialog(HomeActivity homeActivity) {

        dialog = new Dialog(homeActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        logout_tv = (TextView) dialog.findViewById(R.id.logout_tv);
        logCancel_tv = (TextView) dialog.findViewById(R.id.logCancel_tv);

        GET_LISTENER();
    }

    private void GET_LISTENER() {


        logout_tv.setOnClickListener(this);
        logCancel_tv.setOnClickListener(this);

    }

    private void replaceFragment(Fragment fragment) {


        removeFragments();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.commit();

        //    Fragment fragment1 =getSupportFragmentManager().findFragmentById(R.id.container);

        if ( fragment instanceof FavouritesFragment ||fragment instanceof DownloadFragment) {

          //  doubleBackToExitPressedOnce = true;
            doubleBackToExitPressedOnce = false;

        }
        if(fragment instanceof  HomeFragment)
        {
            doubleBackToExitPressedOnce = false;

        }
        if (fragment instanceof SearchFragment) {

            doubleBackToExitPressedOnce = true;
         //   doubleBackToExitPressedOnce = true;

        }


    }

    public void removeFragments() {

        int count = getSupportFragmentManager().getBackStackEntryCount();
        Log.e("Count", "" + count);


        if (count != 0) {
            while (count != 0) {
                getSupportFragmentManager().popBackStackImmediate();
                count = getSupportFragmentManager().getBackStackEntryCount();
                Log.e("InSide Count", "" + count);
            }
        }
    }
    private void setUpTabLayout() {

        tabs_tl.addTab(tabs_tl.newTab().setIcon(R.mipmap.ic_home_selected).setText("Home"));
        tabs_tl.addTab(tabs_tl.newTab().setIcon(R.mipmap.ic_search_unselect).setText("Search"));
        tabs_tl.addTab(tabs_tl.newTab().setIcon(R.mipmap.ic_favorite_unselect).setText("Favorites"));
        tabs_tl.addTab(tabs_tl.newTab().setIcon(R.mipmap.ic_download_unselect).setText("Downloads"));
        tabs_tl.addTab(tabs_tl.newTab().setIcon(R.mipmap.ic_logout_unselect).setText("Logout"));

        LinearLayout linearLayout = (LinearLayout)tabs_tl.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.parseColor("#ffffff"));
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);
    }
    private void getAllWidgets() {

        tabs_tl = (TabLayout) findViewById(R.id.tabs_tl);
        container_fl = (FrameLayout) findViewById(R.id.container);
        homeFragment = new HomeFragment();
        searchFragment = new SearchFragment();
        favouritesFragment = new FavouritesFragment();
        downloadFragment = new DownloadFragment();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.logCancel_tv:

                dialog.dismiss();

                break;

            case R.id.logout_tv:

                SharedPreference.getInstance().clearDataList(getApplicationContext(), SharedPreference.KEY_LIST);
                SharedPreference.getInstance().clearDataList(getApplicationContext(), SharedPreference.GENRE_LIST);
                SharedPreference.getInstance().clearDataList(getApplicationContext(), SharedPreference.INSTRUCTOR_LIST);
                SharedPreference.getInstance().clearData(getApplicationContext(), SharedPreference.API_RESPONSE);

                Intent intent = new Intent(getApplication(), LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                finish();

                break;


        }

    }

    @Override
    public void onBackPressed() {


        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);




        Log.d("dsad", doubleBackToExitPressedOnce + "");

        if (fragment instanceof SearchFragment) {


            if (count == false) {

                doubleBackToExitPressedOnce = false;

            }
        }

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            else
            {
                if(fragment instanceof  HomeFragment)
                {
                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, " Please do one more click on BACK button for exit from App!", Toast.LENGTH_SHORT).show();
                }
                else {
                  //  int count = tabs_tl.getSelectedTabPosition();
                    tabs_tl.getTabAt(0).select();
                }
            }



     /*   if (fragment instanceof SearchFragment) {


            if (count == false) {

                doubleBackToExitPressedOnce = true;

            }
        }


        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        if (fragment instanceof SearchFragment) {
            count = true;

        } else {
            count = false;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, " Please do one more click on BACK button for exit from App!", Toast.LENGTH_SHORT).show();


    }*/
    }
}