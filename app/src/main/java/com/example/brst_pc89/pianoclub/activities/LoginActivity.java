package com.example.brst_pc89.pianoclub.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.ApiClientLogin;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView toggle_iv, remember_iv;
    TextView login_iv;

    EditText userName_et, password_et;

    String uSerName, pAssWord, response_data;

    Dialog dialog, dialognet;
    String shared_data;
    ProgressDialog progressDialog;

    NetworkInfo info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        shared_data = SharedPreference.getInstance().getData(getApplicationContext(), SharedPreference.API_UserName);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        Log.d("sha", shared_data);

        info = (NetworkInfo) ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        GET_UI_ID();

        SET_CLICK_LISTENER();

        if (!shared_data.equals("null")) {

            String userName = SharedPreference.getInstance().getData(getApplicationContext(), SharedPreference.API_UserName);
            String passWord = SharedPreference.getInstance().getData(getApplicationContext(), SharedPreference.API_PassWord);

            userName_et.setText(userName);
            password_et.setText(passWord);
            toggle_iv.setVisibility(View.GONE);
            remember_iv.setVisibility(View.VISIBLE);

        }


    }


    private void GET_UI_ID() {

        toggle_iv = (ImageView) findViewById(R.id.toggle_iv);
        remember_iv = (ImageView) findViewById(R.id.remember_iv);
        login_iv = (TextView) findViewById(R.id.login_iv);
        userName_et = (EditText) findViewById(R.id.userName_et);
        password_et = (EditText) findViewById(R.id.password_et);
    }

    private void SET_CLICK_LISTENER() {

        toggle_iv.setOnClickListener(this);
        login_iv.setOnClickListener(this);
        remember_iv.setOnClickListener(this);

    }

    public void GET_DATA() {

        uSerName = userName_et.getText().toString();
        pAssWord = password_et.getText().toString();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.toggle_iv:

                remember_iv.setVisibility(View.VISIBLE);
                toggle_iv.setVisibility(View.GONE);

                break;

            case R.id.login_iv:

                GET_DATA();

                if (uSerName.isEmpty()) {
                    ERROR_DIALOG(this, 0);
                } else if (pAssWord.isEmpty()) {
                    ERROR_DIALOG(this, 1);
                } else {

                    progressDialog.show();


                    pianoApiInterface apiInterface = ApiClientLogin.getLoginClient().create(pianoApiInterface.class);


                    Call<String> loginApi = apiInterface.logincall(uSerName, pAssWord);
                    loginApi.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {

                            Log.d("response", response.body().toString());

                            progressDialog.dismiss();
                            response_data = response.body();

                            if (remember_iv.getVisibility() == View.VISIBLE) {


                                SharedPreference.getInstance().storeData(getApplicationContext(), SharedPreference.API_UserName, uSerName);
                                SharedPreference.getInstance().storeData(getApplicationContext(), SharedPreference.API_PassWord, pAssWord);
                                SharedPreference.getInstance().storeData(getApplicationContext(), SharedPreference.API_RESPONSE, response_data);
                            }
                            if (toggle_iv.getVisibility() == View.VISIBLE) {

                                SharedPreference.getInstance().clearData(getApplicationContext(), SharedPreference.API_RESPONSE);
                                SharedPreference.getInstance().clearData(getApplicationContext(), SharedPreference.API_UserName);
                                SharedPreference.getInstance().clearData(getApplicationContext(), SharedPreference.API_PassWord);
                            }

                            if (response_data.equals("login_fail")) {

                                ERROR_DIALOG(LoginActivity.this, 2);

                            } else if (response_data.equals("standard")) {
                                ERROR_DIALOG(LoginActivity.this, 3);
                            } else {

                                SharedPreference.getInstance().storeData(getApplicationContext(), SharedPreference.API_UserNameRequest, uSerName);
                                Intent intent = new Intent(getApplication(), HomeActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                finish();

                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    NETWORK_DIALOG(LoginActivity.this);
                                    progressDialog.dismiss();

                                }
                            }, 2000);


                        }
                    });
                }
                break;


            case R.id.remember_iv:

                remember_iv.setVisibility(View.GONE);
                toggle_iv.setVisibility(View.VISIBLE);

                break;

            case R.id.dialogdismiss_tv:

                dialog.dismiss();
                break;

            case R.id.dialogOk_tv:

                dialognet.dismiss();
                break;
        }

    }

    private void NETWORK_DIALOG(LoginActivity loginActivity) {

        dialognet = new Dialog(loginActivity);
        dialognet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialognet.setContentView(R.layout.dialog_filter);
        dialognet.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView content_tv = (TextView) dialognet.findViewById(R.id.content_tv);
        TextView dialogOk_tv = (TextView) dialognet.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Connection or Download failed.");

        dialogOk_tv.setOnClickListener(this);
        dialognet.show();


    }

    private void ERROR_DIALOG(LoginActivity loginActivity, int b) {

        dialog = new Dialog(loginActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView msg_tv = (TextView) dialog.findViewById(R.id.msg_tv);
        TextView dialogdismiss_tv = (TextView) dialog.findViewById(R.id.dialogdismiss_tv);

        dialogdismiss_tv.setOnClickListener(this);

        if (b == 1) {
            msg_tv.setText("Please enter your password.");
        } else if (b == 2) {
            msg_tv.setText("Username or password are not correct.");
        } else if (b == 3) {
            msg_tv.setText("You must be a Premium Member to access this area.");
        }
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplication(), BrowseLoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
