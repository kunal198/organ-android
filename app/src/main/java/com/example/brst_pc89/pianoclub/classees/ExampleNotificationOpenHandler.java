package com.example.brst_pc89.pianoclub.classees;


import android.util.Log;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.onesignal.OneSignal.NotificationOpenedHandler;

/**
 * Created by brst-pc89 on 4/6/17.
 */
public class ExampleNotificationOpenHandler implements NotificationOpenedHandler {


    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        try{
            Log.e("RESULT",""+result);
        }catch (Throwable t){
            t.printStackTrace();
        }
    }
}