package com.example.brst_pc89.pianoclub.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;

public class BannerActivity extends Activity {
    TextView tvSubscribe,tvTitle;
    ImageView backfrag_iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);

        tvSubscribe=(TextView)findViewById(R.id.subscribe_tv);
        tvTitle=(TextView)findViewById(R.id.title);
        backfrag_iv = (ImageView)findViewById(R.id.backfrag_iv);

        tvTitle.setText("Go Premium");

        tvSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.organclubhouse.com/w/membership-benefits/"));
                startActivity(browserIntent);

            }
        });

        backfrag_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
