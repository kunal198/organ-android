package com.example.brst_pc89.pianoclub.classees;

import java.util.ArrayList;

/**
 * Created by brst-pc89 on 4/26/17.
 */
public class GetCategory {


    String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    String NAME;

    public String getNAME() {
        return NAME;
    }

    String DESCRIPTION;

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }
}
