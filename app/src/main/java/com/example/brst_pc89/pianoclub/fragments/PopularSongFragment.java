package com.example.brst_pc89.pianoclub.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.adaptor.PopularAdaptor;
import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopularSongFragment extends Fragment implements View.OnClickListener {

    TextView headerAbove_tv, headerBelow_tv;

    ExpandableListView popularFragment_lv;

    ImageView back_iv, popUp_iv, popUpdismiss_iv;

    PopupWindow popupWindow;
    Dialog dialognet;
    String id,name,description;

    List<HashMap> responseList;
    List<String> dataList;
    ProgressDialog progressDialog;

   // NetworkInfo info;



    boolean showingFirst = false;

//    HomeFragment homeFragment=new HomeFragment();


    public PopularSongFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popular_song, container, false);
        id=getArguments().getString("id");
        name=getArguments().getString("name");
        description=getArguments().getString("description");

        //Log.e("idddd",strtext);

        Log.d("day", dataList + "");
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

      //   info = (NetworkInfo) ((ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        GET_LAYOUT_ID(view);

        DATA_FROM_SERVER();

        GET_LISTENER();

        return view;

    }

    private void NETWORK_DIALOG(Context popularSongFragment) {

        dialognet = new Dialog(popularSongFragment);
        dialognet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialognet.setContentView(R.layout.dialog_filter);
        dialognet.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
     //   dialognet.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView content_tv = (TextView) dialognet.findViewById(R.id.content_tv);
        TextView dialogOk_tv = (TextView) dialognet.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Connection or Download failed.");

        dialogOk_tv.setOnClickListener(this);
        dialognet.show();
    }


    /*......................get data from sertver....................*/

    private void DATA_FROM_SERVER() {

      /*  if(info==null)
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    NETWORK_DIALOG(getContext());
                    progressDialog.dismiss();

                }
            }, 2000);*/




      //  }
        pianoApiInterface apiInterface = ApiClient.getClient().create(pianoApiInterface.class);
        Call<List<HashMap>> call = apiInterface.getJson(Integer.parseInt(id));
        call.enqueue(new Callback<List<HashMap>>() {
            @Override
            public void onResponse(Call<List<HashMap>> call, Response<List<HashMap>> response) {


                responseList = new ArrayList<HashMap>();

                responseList = response.body();

                headerBelow_tv.setVisibility(View.VISIBLE);
                headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by publish date");
                Log.d("hgh", dataList + "");
                PopularAdaptor popularAdaptor = new PopularAdaptor(getContext(), responseList);
                popularFragment_lv.setAdapter(popularAdaptor);
                progressDialog.dismiss();
                if(responseList.size()==0)
                {

                    new AlertDialog.Builder(getActivity())
                            .setTitle("OrganClubhouse").setCancelable(false)
                            .setMessage("No videos found")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();

                }

            }

            @Override
            public void onFailure(Call<List<HashMap>> call, Throwable t) {


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        NETWORK_DIALOG(getContext());
                        progressDialog.dismiss();

                    }
                }, 2000);

            }
        });
    }

    private void SHOW_POPUP(View v) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_popup, null);
        TextView title_tv = (TextView) popupView.findViewById(R.id.title_tv);
        TextView artist_tv = (TextView) popupView.findViewById(R.id.artist_tv);
        TextView date_tv = (TextView) popupView.findViewById(R.id.date_tv);

        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //TODO do sth here on dismiss
                popupWindow.dismiss();
            }
        });

        popupWindow.showAsDropDown(v, 0, -40);

        popUp_iv.setVisibility(View.GONE);
        popUpdismiss_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                popUp_iv.setVisibility(View.VISIBLE);
                popUpdismiss_iv.setVisibility(View.GONE);

            }
        });
        title_tv.setOnClickListener(this);
        artist_tv.setOnClickListener(this);
        date_tv.setOnClickListener(this);


    }


    /*.................. click listener on button......................*/

    private void GET_LISTENER()

    {
        back_iv.setOnClickListener(this);
        popUp_iv.setOnClickListener(this);

    }


   /* ............................get the id of all view from layout..................................*/

    private void GET_LAYOUT_ID(View view) {
        headerAbove_tv = (TextView) view.findViewById(R.id.headerAbove_tv);
        headerBelow_tv = (TextView) view.findViewById(R.id.headerBelow_tv);
        popularFragment_lv = (ExpandableListView) view.findViewById(R.id.popularFragment_lv);
        back_iv = (ImageView) view.findViewById(R.id.back_iv);
        popUp_iv = (ImageView) view.findViewById(R.id.popUp_iv);
        popUpdismiss_iv = (ImageView) view.findViewById(R.id.popUpdismiss_iv);
        dataList = new ArrayList<String>();

        headerAbove_tv.setText(name);
        //   headerBelow_tv.setText("599 videos, sorted by publish date");

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_iv:


                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                break;

            case R.id.popUp_iv:

                SHOW_POPUP(popUp_iv);
                break;

            case R.id.title_tv:

                if (showingFirst == false) {
                    SORT_LIST("title");
                    showingFirst = true;
                } else {

                    DES_LIST("title");

                    showingFirst = false;
                }
                break;

            case R.id.artist_tv:


                if (showingFirst == false) {
                    SORT_LIST("artist");
                    showingFirst = true;
                } else {

                    DES_LIST("artist");

                    showingFirst = false;
                }

                break;

            case R.id.date_tv:

                if (showingFirst == false) {
                    SORT_LIST("dateformat");
                    showingFirst = true;
                } else {

                    DES_LIST("dateformat");

                    showingFirst = false;
                }


                break;

            case R.id.dialogOk_tv:
                dialognet.dismiss();
                break;

        }

    }

    private void DES_LIST(final String string) {

        Collections.sort(responseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return secondValue.compareToIgnoreCase(firstValue);
            }
        });
        PopularAdaptor popularAdaptor = new PopularAdaptor(getContext(), responseList);
        popularFragment_lv.setAdapter(popularAdaptor);
        if (string.equals("title")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }


    public void SORT_LIST(final String string) {

        Collections.sort(responseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return firstValue.compareToIgnoreCase(secondValue);
            }
        });
        PopularAdaptor popularAdaptor = new PopularAdaptor(getContext(), responseList);
        popularFragment_lv.setAdapter(popularAdaptor);
        if (string.equals("title")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }


}
