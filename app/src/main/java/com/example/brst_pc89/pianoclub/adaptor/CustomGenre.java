package com.example.brst_pc89.pianoclub.adaptor;

/**
 * Created by brst-pc80 on 6/5/17.
 */

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.Model.Category;
import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;

import static com.example.brst_pc89.pianoclub.R.id.genrePop_iv;

public class CustomGenre extends BaseAdapter {
    Context context;
    ArrayList<Category> arrayListCategory;
    ArrayList<String> genreList;

    public CustomGenre(Context context,ArrayList<Category> arrayListCategory,ArrayList<String> genreList)
    {
        this.context = context;
        this.arrayListCategory=arrayListCategory;
        this.genreList=genreList;
    }



    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Category category=arrayListCategory.get(position);
        holder.txtTitle.setText(category.getName());


        final ViewHolder finalHolder = holder;

        if(genreList.contains(arrayListCategory.get(position).getId()))
            finalHolder.imageView.setImageLevel(20);

        //finalHolder.imageView.setImageLevel(10);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(finalHolder.imageView.getDrawable().getLevel()==20)
                {
                    finalHolder.imageView.setImageLevel(10);

                    if(genreList.contains(arrayListCategory.get(position).getId())) {
                        genreList.remove(arrayListCategory.get(position).getId());
                        SharedPreference.getInstance().storeArrayList(context, SharedPreference.GENRE_LIST, genreList);
                        SharedPreference.getInstance().storeArrayList(context, SharedPreference.GENRE_LISTNAME, genreList);
                    }
                }
                else
                {
                    finalHolder.imageView.setImageLevel(20);
                    genreList.add(arrayListCategory.get(position).getId());
                    SharedPreference.getInstance().storeArrayList(context, SharedPreference.GENRE_LIST, genreList);
                }
            }
        });
        /*RowItem rowItem = (RowItem) getItem(position);

        holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return arrayListCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return arrayListCategory.indexOf(getItem(position));
    }
}