package com.example.brst_pc89.pianoclub.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;

/**
 * Created by brst-pc89 on 3/22/17.
 */
public class GenerAdaptor extends BaseAdapter {

    String[] tutorials;
    Context context;
    LayoutInflater inflater;
    public GenerAdaptor(Context context, String[] tutorials) {
        this.context=context;
        this.tutorials=tutorials;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return tutorials.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=inflater.inflate(R.layout.custom_genrelayout,null);
        TextView genre_tv=(TextView)view.findViewById(R.id.genre_tv);
        genre_tv.setText(tutorials[position]);
        return view;
    }
}
