package com.example.brst_pc89.pianoclub.classees;

/**
 * Created by brst-pc89 on 4/13/17.
 */
public class GetVersion {

    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String andriod_version_id;

    public String getAndriod_version_id() {
        return andriod_version_id;
    }

    public void setAndriod_version_id(String andriod_version_id) {
        this.andriod_version_id = andriod_version_id;



    }

    String maintaince_mode;

    public String getMaintaince_mode() {
        return maintaince_mode;
    }

    public void setMaintaince_mode(String maintaince_mode) {
        this.maintaince_mode = maintaince_mode;
    }
}
