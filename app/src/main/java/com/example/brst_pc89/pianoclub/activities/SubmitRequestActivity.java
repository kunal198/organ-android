package com.example.brst_pc89.pianoclub.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.adaptor.SpinnerAdapter;
import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.GetCategory;
import com.example.brst_pc89.pianoclub.classees.RequestClass;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubmitRequestActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner spinner;
    TextView requestsubmit_tv;
    EditText song_et, artist_et, des_et;
    TextInputLayout song_input_layout, artist_input_layout, des_input_layout;
    Dialog dialog, dialognet;
    LinearLayout linearLayout;
    ProgressDialog progressDialog;
    String defaultTextForSpinner = "Select Category", categoryList;
    ArrayList<String> IDList=new ArrayList<>();
    int spinner_val;

    String dropdown_list[] = {"Popular Songs", "Christian", "Gospel", "T.V Theme", "Christmas", "Gospel Organ", "Technique", "Theory", "Browse All"};

    List<HashMap> dropdown = new ArrayList<>();
    List<String> drop = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        SET_VIEWS();
        GET_DATA_SERVER();


        requestsubmit_tv.setOnClickListener(this);
        linearLayout.setOnClickListener(this);

    }

    private void GET_DATA_SERVER() {

        progressDialog.show();

        pianoApiInterface apiInterface = ApiClient.getClient().create(pianoApiInterface.class);
        Call<List<HashMap>> category = apiInterface.category();
        category.enqueue(new Callback<List<HashMap>>() {
            @Override
            public void onResponse(Call<List<HashMap>> call, Response<List<HashMap>> response) {

                progressDialog.dismiss();
                dropdown = response.body();


                for (int i = 0; i < dropdown.size(); i++) {
                    categoryList = response.body().get(i).get("NAME").toString();
                    IDList.add(response.body().get(i).get("ID").toString());
                    drop.add(categoryList);

                }

                SpinnerAdapter adapter = new SpinnerAdapter(SubmitRequestActivity.this, android.R.layout.simple_list_item_1);
                adapter.addAll(drop);
                adapter.add("Select Category");
                spinner.setAdapter(adapter);
                spinner.setSelection(adapter.getCount());

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {
                        String selectedItem = parent.getItemAtPosition(position).toString();
                        try {
                            spinner_val = Integer.parseInt(IDList.get(position));
                        }
                        catch (Exception e)
                        {

                        }

                    } // to close the onItemSelected
                    public void onNothingSelected(AdapterView<?> parent)
                    {

                    }
                });

            }

            @Override
            public void onFailure(Call<List<HashMap>> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        NETWORK_DIALOG(SubmitRequestActivity.this);
                        progressDialog.dismiss();
                    }
                }, 2000);
            }
        });
    }
    private void SET_VIEWS() {

        spinner = (Spinner) findViewById(R.id.spinner);
        requestsubmit_tv = (TextView) findViewById(R.id.requestsubmit_tv);
        song_et = (EditText) findViewById(R.id.song_et);
        artist_et = (EditText) findViewById(R.id.artist_et);
      //  des_et = (EditText) findViewById(R.id.des_et);
        song_input_layout = (TextInputLayout) findViewById(R.id.song_input_layout);
        ///  des_input_layout = (TextInputLayout) findViewById(R.id.des_input_layout);
        artist_input_layout = (TextInputLayout) findViewById(R.id.artist_input_layout);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.requestsubmit_tv:

                String spinnerValue = spinner.getSelectedItem().toString();
                String songName = song_input_layout.getEditText().getText().toString();
                String artistName = artist_input_layout.getEditText().getText().toString();
                //  String descriptionName = des_input_layout.getEditText().getText().toString();
                String userName = SharedPreference.getInstance().getData(getApplicationContext(), SharedPreference.API_UserNameRequest);
                Log.d("username", userName);

                /*if (spinnerValue.equals("Christian")) {
                    spinner_val = 7;
                } else if (spinnerValue.equals("Christmas technique")) {
                    spinner_val = 11;
                } else if (spinnerValue.equals("Gospel")) {
                    spinner_val = 6;
                } else if (spinnerValue.equals("Organ")) {
                    spinner_val = 12;
                } else if (spinnerValue.equals("Other")) {
                    spinner_val = 10;
                } else if (spinnerValue.equals("Popular Hit Songs")) {
                    spinner_val = 1;
                } else if (spinnerValue.equals("Technique/Tricks/Lic")) {
                    spinner_val = 5;
                } else if (spinnerValue.equals("Television")) {
                    spinner_val = 8;
                } else if (spinnerValue.equals("Theory/Chords")) {
                    spinner_val = 4;
                } else if (spinnerValue.equals("VIDEOS")) {
                    spinner_val = 2;
                }*/


                if (spinnerValue.equals("Select Category")) {
                    Snackbar snackbar = Snackbar.make(v, "Please select category from dropdown list.", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    view.setBackgroundColor(getResources().getColor(R.color.colorsnakebar));
                    snackbar.show();
                } else if (songName.isEmpty()) {

                    Snackbar snackbar = Snackbar.make(v, "Please enter song name.", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    view.setBackgroundColor(getResources().getColor(R.color.colorsnakebar));
                    snackbar.show();
                } /*else if (artistName.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(v, "Please enter artist.", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    view.setBackgroundColor(getResources().getColor(R.color.colorsnakebar));
                    snackbar.show();
                } else if (descriptionName.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(v, "Please enter description.", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    view.setBackgroundColor(getResources().getColor(R.color.colorsnakebar));
                    snackbar.show();*/
                // }

                else {
                   /* Intent intent=new Intent(getApplication(),SubmittionActivity.class);
                    startActivity(intent);
*/

                    progressDialog.show();
                    Log.d("dfd", spinner_val + " " + songName + " " + userName + " " + artistName);
                    pianoApiInterface apiInterface = ApiClient.getClient().create(pianoApiInterface.class);
                    Call<RequestClass> requestCall = apiInterface.submitRequest(spinner_val, songName, userName, artistName);
                    requestCall.enqueue(new Callback<RequestClass>() {
                        @Override
                        public void onResponse(Call<RequestClass> call, Response<RequestClass> response) {


                            Log.d("response", response.body().getMessage());
                            String status = response.body().getStatus();
                            String messsage = response.body().getMessage();
                            if (status.equals("Success") && messsage.equals("Request Saved Successfully")) {
                                progressDialog.dismiss();
                                SUBMIT_DIALOG(SubmitRequestActivity.this);
                            }
                        }

                        @Override
                        public void onFailure(Call<RequestClass> call, Throwable t) {
                            Log.d("sdfd", t.getMessage());

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    NETWORK_DIALOG(SubmitRequestActivity.this);
                                    progressDialog.dismiss();

                                }
                            }, 2000);
                        }
                    });


                }
                break;
            case R.id.dialogdismiss_tv:

                dialog.dismiss();
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                break;

            case R.id.linearLayout:

                if (!(linearLayout instanceof TextInputLayout)) {

                    linearLayout.setOnTouchListener(new View.OnTouchListener() {

                        public boolean onTouch(View v, MotionEvent event) {
                            hideSoftKeyboard();
                            return false;
                        }
                    });
                }
                break;

            case R.id.dialogOk_tv:

                dialognet.dismiss();
        }

    }

    private void hideSoftKeyboard() {

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void SUBMIT_DIALOG(SubmitRequestActivity submitRequestActivity)

    {

        dialog = new Dialog(submitRequestActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView msg_tv = (TextView) dialog.findViewById(R.id.msg_tv);
        TextView content_tv = (TextView) dialog.findViewById(R.id.content_tv);
        TextView dialogdismiss_tv = (TextView) dialog.findViewById(R.id.dialogdismiss_tv);

        dialogdismiss_tv.setOnClickListener(this);
        content_tv.setText("Request received");
        msg_tv.setText("Thank You For Your submission");

        dialog.show();

    }

    private void NETWORK_DIALOG(SubmitRequestActivity submitRequestActivity) {

        dialognet = new Dialog(submitRequestActivity);
        dialognet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialognet.setContentView(R.layout.dialog_filter);
        dialognet.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView content_tv = (TextView) dialognet.findViewById(R.id.content_tv);
        TextView dialogOk_tv = (TextView) dialognet.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Connection or Download failed.");

        dialogOk_tv.setOnClickListener(this);
        dialognet.show();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);

    }
}
