package com.example.brst_pc89.pianoclub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class KeyFragment extends Fragment implements View.OnClickListener {


    ImageView backfrag_iv;
    ImageView keyImageA_iv, keyImageDE_iv, keyImageAB_iv, keyImageE_iv,
            keyImageB_iv, keyImageF_iv, keyImageC_iv,
            keyImageFG_iv, keyImageCD_iv, keyImageG_iv, keyImageD_iv, keyImageGA_iv, keyImageNoKey_iv;

  public static  List<String>  list=new ArrayList<String>();
    public KeyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_key, container, false);

        backfrag_iv = (ImageView) view.findViewById(R.id.backfrag_iv);
        keyImageA_iv = (ImageView) view.findViewById(R.id.keyImageA_iv);
        keyImageDE_iv = (ImageView) view.findViewById(R.id.keyImageDE_iv);
        keyImageAB_iv = (ImageView) view.findViewById(R.id.keyImageAB_iv);
        keyImageE_iv = (ImageView) view.findViewById(R.id.keyImageE_iv);
        keyImageB_iv = (ImageView) view.findViewById(R.id.keyImageB_iv);
        keyImageF_iv = (ImageView) view.findViewById(R.id.keyImageF_iv);
        keyImageC_iv = (ImageView) view.findViewById(R.id.keyImageC_iv);
        keyImageFG_iv = (ImageView) view.findViewById(R.id.keyImageFG_iv);
        keyImageCD_iv = (ImageView) view.findViewById(R.id.keyImageCD_iv);
        keyImageG_iv = (ImageView) view.findViewById(R.id.keyImageG_iv);
        keyImageD_iv = (ImageView) view.findViewById(R.id.keyImageD_iv);
        keyImageGA_iv = (ImageView) view.findViewById(R.id.keyImageGA_iv);
        keyImageNoKey_iv = (ImageView) view.findViewById(R.id.keyImageNoKey_iv);

        list= SharedPreference.getInstance().geKeyList(getContext(),SharedPreference.KEY_LIST);


        GET_SELECTED_BTN();



        backfrag_iv.setOnClickListener(this);
        keyImageA_iv.setOnClickListener(this);
        keyImageDE_iv.setOnClickListener(this);
        keyImageAB_iv.setOnClickListener(this);
        keyImageE_iv.setOnClickListener(this);
        keyImageB_iv.setOnClickListener(this);
        keyImageF_iv.setOnClickListener(this);
        keyImageC_iv.setOnClickListener(this);
        keyImageFG_iv.setOnClickListener(this);
        keyImageCD_iv.setOnClickListener(this);
        keyImageG_iv.setOnClickListener(this);
        keyImageD_iv.setOnClickListener(this);
        keyImageGA_iv.setOnClickListener(this);
        keyImageNoKey_iv.setOnClickListener(this);



        return view;
    }

    private void GET_SELECTED_BTN() {


        if(list.contains(String.valueOf(10)))
            keyImageA_iv.setImageLevel(20);

        if(list.contains(String.valueOf(4)))
            keyImageDE_iv.setImageLevel(20);

        if(list.contains(String.valueOf(11)))
            keyImageAB_iv.setImageLevel(20);

        if(list.contains(String.valueOf(5)))
            keyImageE_iv.setImageLevel(20);

        if(list.contains(String.valueOf(12)))
            keyImageB_iv.setImageLevel(20);

        if(list.contains(String.valueOf(6)))
            keyImageF_iv.setImageLevel(20);

        if(list.contains(String.valueOf(1)))
            keyImageC_iv.setImageLevel(20);

        if(list.contains(String.valueOf(7)))
            keyImageFG_iv.setImageLevel(20);

        if(list.contains(String.valueOf(2)))
            keyImageCD_iv.setImageLevel(20);

        if(list.contains(String.valueOf(8)))
            keyImageG_iv.setImageLevel(20);

        if(list.contains(String.valueOf(3)))
            keyImageD_iv.setImageLevel(20);

        if(list.contains(String.valueOf(9)))
            keyImageGA_iv.setImageLevel(20);

        if(list.contains(String.valueOf(0)))
            keyImageNoKey_iv.setImageLevel(20);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.keyImageA_iv:


                if(keyImageA_iv.getDrawable().getLevel()==20)
                {

                    keyImageA_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(10))) {

                        list.remove(String.valueOf(10));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageA_iv.setImageLevel(20);
                    list.add(String.valueOf(10));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }

                break;

            case R.id.keyImageDE_iv:



                if(keyImageDE_iv.getDrawable().getLevel()==20)
                {

                    keyImageDE_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(4))) {

                        list.remove(String.valueOf(4));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageDE_iv.setImageLevel(20);
                    list.add(String.valueOf(4));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }

             //   keyImageDE_iv.setImageLevel(20);
                break;

            case R.id.keyImageAB_iv:

                if(keyImageAB_iv.getDrawable().getLevel()==20)
                {

                    keyImageAB_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(11))) {

                        list.remove(String.valueOf(11));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageAB_iv.setImageLevel(20);
                    list.add(String.valueOf(11));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageE_iv:

                if(keyImageE_iv.getDrawable().getLevel()==20)
                {

                    keyImageE_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(5))) {

                        list.remove(String.valueOf(5));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageE_iv.setImageLevel(20);
                    list.add(String.valueOf(5));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageB_iv:

                if(keyImageB_iv.getDrawable().getLevel()==20)
                {

                    keyImageB_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(12))) {

                        list.remove(String.valueOf(12));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageB_iv.setImageLevel(20);
                    list.add(String.valueOf(12));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageF_iv:

                if(keyImageF_iv.getDrawable().getLevel()==20)
                {

                    keyImageF_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(6))) {

                        list.remove(String.valueOf(6));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageF_iv.setImageLevel(20);
                    list.add(String.valueOf(6));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageC_iv:

                if(keyImageC_iv.getDrawable().getLevel()==20)
                {

                    keyImageC_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(1))) {

                        list.remove(String.valueOf(1));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageC_iv.setImageLevel(20);
                    list.add(String.valueOf(1));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageFG_iv:

                if(keyImageFG_iv.getDrawable().getLevel()==20)
                {

                    keyImageFG_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(7))) {

                        list.remove(String.valueOf(7));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageFG_iv.setImageLevel(20);
                    list.add(String.valueOf(7));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageCD_iv:

                if(keyImageCD_iv.getDrawable().getLevel()==20)
                {

                    keyImageCD_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(2))) {

                        list.remove(String.valueOf(2));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageCD_iv.setImageLevel(20);
                    list.add(String.valueOf(2));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageG_iv:

                if(keyImageG_iv.getDrawable().getLevel()==20)
                {

                    keyImageG_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(8))) {

                        list.remove(String.valueOf(8));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageG_iv.setImageLevel(20);
                    list.add(String.valueOf(8));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageD_iv:

                if(keyImageD_iv.getDrawable().getLevel()==20)
                {

                    keyImageD_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(3))) {

                        list.remove(String.valueOf(3));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageD_iv.setImageLevel(20);
                    list.add(String.valueOf(3));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageGA_iv:

                if(keyImageGA_iv.getDrawable().getLevel()==20)
                {

                    keyImageGA_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(9))) {

                        list.remove(String.valueOf(9));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageGA_iv.setImageLevel(20);
                    list.add(String.valueOf(9));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                }
                break;

            case R.id.keyImageNoKey_iv:

                if(keyImageNoKey_iv.getDrawable().getLevel()==20)
                {

                    keyImageNoKey_iv.setImageLevel(10);

                    if(list.contains(String.valueOf(0))) {

                        list.remove(String.valueOf(0));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    }

                }
                else
                {
                    keyImageNoKey_iv.setImageLevel(20);
                    list.add(String.valueOf(0));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                    Log.d("list",list+"");
                }
                break;

            case R.id.backfrag_iv:


           //     SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.KEY_LIST, list);

                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;

        }

    }
}
