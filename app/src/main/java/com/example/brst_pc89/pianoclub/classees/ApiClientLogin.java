package com.example.brst_pc89.pianoclub.classees;

import retrofit2.Retrofit;

import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class ApiClientLogin {

    //public static final String BASE_URL =  "http://pianoclubhouse.com/";

    public static final String BASE_URL =  "http://organclubhouse.com/";

    private static Retrofit retrofit = null;

    public static Retrofit getLoginClient() {

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
