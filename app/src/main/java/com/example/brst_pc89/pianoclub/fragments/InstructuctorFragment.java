package com.example.brst_pc89.pianoclub.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstructuctorFragment extends Fragment implements View.OnClickListener {

    ImageView backfrag_iv, instTj_iv, instCar_iv,
            instDar_iv, instSh_iv, instPeter_iv, instRic_iv, instDavid_iv,instStephano_iv,instBrice_iv,instjerm_iv,instGogo_iv;
    List<String> InstructorList;

    public InstructuctorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_instructuctor, container, false);

        backfrag_iv = (ImageView) view.findViewById(R.id.backfrag_iv);
        instTj_iv = (ImageView) view.findViewById(R.id.instTj_iv);
        instCar_iv = (ImageView) view.findViewById(R.id.instCar_iv);
        instDar_iv = (ImageView) view.findViewById(R.id.instDar_iv);
        instSh_iv = (ImageView) view.findViewById(R.id.instSh_iv);
        instRic_iv = (ImageView) view.findViewById(R.id.instRic_iv);
        instPeter_iv = (ImageView) view.findViewById(R.id.instPeter_iv);
        instDavid_iv = (ImageView) view.findViewById(R.id.instDavid_iv);
        instStephano_iv = (ImageView) view.findViewById(R.id.instStephano_iv);
        instBrice_iv = (ImageView) view.findViewById(R.id.instBrice_iv);
        instjerm_iv = (ImageView) view.findViewById(R.id.instjerm_iv);
        instGogo_iv = (ImageView) view.findViewById(R.id.instGogo_iv);

        InstructorList=new ArrayList<String>();

        InstructorList= SharedPreference.getInstance().geInstructorList(getContext(),SharedPreference.INSTRUCTOR_LIST);

        GET_SELECTED_BTN();

        instTj_iv.setOnClickListener(this);
        instCar_iv.setOnClickListener(this);
        instDar_iv.setOnClickListener(this);
        instSh_iv.setOnClickListener(this);
        instRic_iv.setOnClickListener(this);
        instPeter_iv.setOnClickListener(this);
        instDavid_iv.setOnClickListener(this);
        backfrag_iv.setOnClickListener(this);
        instStephano_iv.setOnClickListener(this);
        instBrice_iv.setOnClickListener(this);
        instjerm_iv.setOnClickListener(this);
        instGogo_iv.setOnClickListener(this);
        return view;
    }

    private void GET_SELECTED_BTN() {

        if(InstructorList.contains(String.valueOf(0)))
            instTj_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(1)))
            instCar_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(2)))
            instDar_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(3)))
            instSh_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(4)))
            instRic_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(5)))
            instDavid_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(6)))
            instGogo_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(7)))
            instPeter_iv.setImageLevel(20);

        if(InstructorList.contains(String.valueOf(8)))
            instStephano_iv.setImageLevel(20);


        if(InstructorList.contains(String.valueOf(9)))
            instBrice_iv.setImageLevel(20);
        if(InstructorList.contains(String.valueOf(10)))
            instjerm_iv.setImageLevel(20);

//        if(InstructorList.contains(7))
//            keyImageFG_iv.setImageLevel(20);
//
//        if(InstructorList.contains(2))
//            keyImageCD_iv.setImageLevel(20);
//
//        if(InstructorList.contains(8))
//            keyImageG_iv.setImageLevel(20);
//
//        if(InstructorList.contains(3))
//            keyImageD_iv.setImageLevel(20);
//
//        if(InstructorList.contains(9))
//            keyImageGA_iv.setImageLevel(20);
//
//        if(InstructorList.contains(0))
//            keyImageNoKey_iv.setImageLevel(20);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.backfrag_iv:
           //     SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;

            case R.id.instTj_iv:

                if(instTj_iv.getDrawable().getLevel()==20)
                {

                    instTj_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(0))) {

                        InstructorList.remove(String.valueOf(0));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instTj_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(0));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

            case R.id.instCar_iv:

                if(instCar_iv.getDrawable().getLevel()==20)
                {

                    instCar_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(1))) {

                        InstructorList.remove(String.valueOf(1));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instCar_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(1));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

            case R.id.instDar_iv:
                if(instDar_iv.getDrawable().getLevel()==20)
                {

                    instDar_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(2))) {

                        InstructorList.remove(String.valueOf(2));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instDar_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(2));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

            case R.id.instSh_iv:
                if(instSh_iv.getDrawable().getLevel()==20)
                {

                    instSh_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(3))) {

                        InstructorList.remove(String.valueOf(3));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instSh_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(3));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

            case R.id.instRic_iv:

                if(instRic_iv.getDrawable().getLevel()==20)
                {

                    instRic_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(4))) {

                        InstructorList.remove(String.valueOf(4));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }
                }
                else
                {
                    instRic_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(4));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

            case R.id.instDavid_iv:

                if(instDavid_iv.getDrawable().getLevel()==20)
                {

                    instDavid_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(5))) {

                        InstructorList.remove(String.valueOf(5));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instDavid_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(5));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

            case R.id.instGogo_iv:

                if(instGogo_iv.getDrawable().getLevel()==20)
                {

                    instGogo_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(6))) {

                        InstructorList.remove(String.valueOf(6));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instGogo_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(6));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

            case R.id.instPeter_iv:
                if(instPeter_iv.getDrawable().getLevel()==20)
                {

                    instPeter_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(7))) {

                        InstructorList.remove(String.valueOf(7));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instPeter_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(7));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;



            case R.id.instStephano_iv:
                if(instStephano_iv.getDrawable().getLevel()==20)
                {

                    instStephano_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(8))) {

                        InstructorList.remove(String.valueOf(8));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instStephano_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(8));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;


            case R.id.instBrice_iv:
                if(instBrice_iv.getDrawable().getLevel()==20)
                {

                    instBrice_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(9))) {

                        InstructorList.remove(String.valueOf(9));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instBrice_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(9));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;


            case R.id.instjerm_iv:
                if(instjerm_iv.getDrawable().getLevel()==20)
                {

                    instjerm_iv.setImageLevel(10);

                    if(InstructorList.contains(String.valueOf(10))) {

                        InstructorList.remove(String.valueOf(10));
                        SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    }

                }
                else
                {
                    instjerm_iv.setImageLevel(20);
                    InstructorList.add(String.valueOf(10));
                    SharedPreference.getInstance().storeArrayList(getContext(), SharedPreference.INSTRUCTOR_LIST, InstructorList);
                    Log.d("list",InstructorList+"");
                }
                break;

        }

    }
}
