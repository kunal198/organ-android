package com.example.brst_pc89.pianoclub.classees;

/**
 * Created by brst-pc89 on 4/21/17.
 */
public class RequestClass {

    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
