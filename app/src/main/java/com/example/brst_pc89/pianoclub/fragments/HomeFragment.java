package com.example.brst_pc89.pianoclub.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.Model.Category;
import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.activities.SubmitRequestActivity;
import com.example.brst_pc89.pianoclub.adaptor.HomeAdaaptor;
import com.example.brst_pc89.pianoclub.adaptor.SpinnerAdapter;
import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.SimpleViewAnimator;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    GridView home_gv;
    ImageView submit_request_iv;
    TextView submit_request_tv;
    PopularSongFragment popularSongFragment = new PopularSongFragment();
    ChristianFragment christianFragment = new ChristianFragment();
    GospelFragment gospelFragment = new GospelFragment();
    ThemeFragment themeFragment = new ThemeFragment();
    ChristmasFragment christmasFragment = new ChristmasFragment();
    OrganFragment organFragment = new OrganFragment();
    TechniqueFragment techniqueFragment = new TechniqueFragment();
    TheoryFragment theoryFragment = new TheoryFragment();
    BrowserFragment browserFragment = new BrowserFragment();
    ProgressDialog progressDialog;
    ArrayList<Category> arrayListCategory=new ArrayList<>();
    HomeAdaaptor homeAdaaptor;

    private int images[] = {R.mipmap.img_popular,
            R.mipmap.img_christian,
            R.mipmap.img_gospel,
            R.mipmap.img_tvtheme,
            R.mipmap.img_christmas,
            R.mipmap.img_organ,
            R.mipmap.img_technique,
            R.mipmap.img_theory,
            R.mipmap.img_browse
    };
    public HomeFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        submit_request_iv = (ImageView) view.findViewById(R.id.submit_request_iv);

        home_gv = (GridView) view.findViewById(R.id.home_gv);


        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        homeAdaaptor = new HomeAdaaptor(getActivity(), arrayListCategory);
        home_gv.setAdapter(homeAdaaptor);
        GET_DATA_SERVER();

        home_gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                replaceFragment(popularSongFragment,arrayListCategory.get(position));


             /*   switch (position) {
                    case 0:
                        replaceFragment(popularSongFragment,arrayListCategory);
                        break;

                    case 1:
                        replaceFragment(christianFragment);
                        break;
                    case 2:

                        replaceFragment(gospelFragment);
                        break;
                    case 3:

                        replaceFragment(themeFragment);
                        break;
                    case 4:

                        replaceFragment(christmasFragment);
                        break;
                    case 5:

                        replaceFragment(organFragment);
                        break;
                    case 6:

                        replaceFragment(techniqueFragment);
                        break;
                    case 7:

                        replaceFragment(theoryFragment);
                        break;
                    case 8:

                        replaceFragment(browserFragment);
                        break;
                }*/

            }
        });


        submit_request_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SubmitRequestActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
        return view;
    }

    private void replaceFragment(Fragment fragment,Category category) {

        Bundle bundle=new Bundle();
        bundle.putString("id", category.getId());
        bundle.putString("name", category.getName());
        bundle.putString("description", category.getDescription());

        //set Fragmentclass Arguments
        //Fragmentclass fragobj=new Fragmentclass();
        //fragobj.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);
        fragment.setArguments(bundle);
        transaction.replace(R.id.container, fragment);
        //transaction.setArguments(bundle);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private void GET_DATA_SERVER() {

        progressDialog.show();

        pianoApiInterface apiInterface = ApiClient.getClient().create(pianoApiInterface.class);
        Call<List<HashMap>> category = apiInterface.category();
        category.enqueue(new Callback<List<HashMap>>() {
            @Override
            public void onResponse(Call<List<HashMap>> call, Response<List<HashMap>> response) {

                progressDialog.dismiss();
                //dropdown = response.body();
                Category category;
                arrayListCategory.clear();
               category=new Category();
                category.setDescription("Desc");
                category.setName("Browse All Videos");
                category.setId("0");
                arrayListCategory.add(category);

                for (int i = 0; i < response.body().size(); i++) {

                    category = new Category();
                    category.setName(response.body().get(i).get("NAME").toString());
                    category.setId(response.body().get(i).get("ID").toString());
                    category.setDescription(response.body().get(i).get("DESCRIPTION").toString());
                    arrayListCategory.add(category);
                }
                Collections.sort(arrayListCategory,new MyCategory());
                homeAdaaptor.notifyDataSetChanged();

                Log.e("sizesss", arrayListCategory.size() + "");

            }

            @Override
            public void onFailure(Call<List<HashMap>> call, Throwable t) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        progressDialog.dismiss();

                    }
                }, 2000);
            }
        });

    }

    class MyCategory implements Comparator<Category> {

        @Override
        public int compare(Category e1, Category e2) {
            if(Integer.parseInt(e1.getId()) > Integer.parseInt(e2.getId())){
                return 1;
            } else {
                return -1;
            }
        }
    }

}
