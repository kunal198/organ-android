package com.example.brst_pc89.pianoclub.fragments;


import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.adaptor.AdaptorFavourite;
import com.example.brst_pc89.pianoclub.adaptor.BrowserAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.ChristmasAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.ThemeAdaptor;
import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavouritesFragment extends Fragment implements View.OnClickListener {

    ExpandableListView favorite_lv;
    List<HashMap> list;
    //  List<HashMap> databaseList;


    ImageView back_iv, popUp_iv, popUpdismiss_iv;
    public static TextView headerBelow_tv;
    public static  PopupWindow popupWindow;
    boolean showingFirst = false;


    public FavouritesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_favourites, container, false);


        GET_LAYOUT_ID(view);

        GET_LISTENER();

        SELECT_DATA();

        return view;
    }

    public void SELECT_DATA() {

        list = SqliteDataBase.SELECT_DATA(getContext());
        Log.d("ds", list + "");
        //    databaseList = SqliteDataBase.SELECT_DOWNLOAD_VIDEO(getContext());
        headerBelow_tv.setText(list.size() + " " + "videos, sorted by publish date");
        AdaptorFavourite adaptorFavourite = new AdaptorFavourite(getActivity(), list);
        favorite_lv.setAdapter(adaptorFavourite);

    }

    private void GET_LAYOUT_ID(View view) {

        favorite_lv = (ExpandableListView) view.findViewById(R.id.favorite_lv);
        headerBelow_tv = (TextView) view.findViewById(R.id.headerBelow_tv);
        headerBelow_tv.setVisibility(View.VISIBLE);
        back_iv = (ImageView) view.findViewById(R.id.back_iv);
        popUp_iv = (ImageView) view.findViewById(R.id.popUp_iv);
        popUpdismiss_iv = (ImageView) view.findViewById(R.id.popUpdismiss_iv);
        back_iv.setVisibility(View.GONE);
        list = new ArrayList<HashMap>();
        //  databaseList = new ArrayList<HashMap>();

    }


    private void GET_LISTENER() {

        back_iv.setOnClickListener(this);
        popUp_iv.setOnClickListener(this);


    }


    private void showPopup(ImageView popUp) {

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_popup, null);
        TextView title_tv = (TextView) popupView.findViewById(R.id.title_tv);
        TextView artist_tv = (TextView) popupView.findViewById(R.id.artist_tv);
        TextView date_tv = (TextView) popupView.findViewById(R.id.date_tv);

        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //TODO do sth here on dismiss
                popupWindow.dismiss();
            }
        });

        popupWindow.showAsDropDown(popUp, 0, -40);

        popUp_iv.setVisibility(View.GONE);
        popUpdismiss_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                popUp_iv.setVisibility(View.VISIBLE);
                popUpdismiss_iv.setVisibility(View.GONE);

            }
        });

        title_tv.setOnClickListener(this);
        artist_tv.setOnClickListener(this);
        date_tv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.popUp_iv:
                showPopup(popUp_iv);
                break;

            case R.id.title_tv:

                if (showingFirst == false) {
                    SORT_LIST("title");
                    showingFirst = true;
                } else {

                    DES_LIST("title");

                    showingFirst = false;
                }
                break;

            case R.id.artist_tv:


                if (showingFirst == false) {
                    SORT_LIST("artist");
                    showingFirst = true;
                } else {

                    DES_LIST("artist");

                    showingFirst = false;
                }

                break;

            case R.id.date_tv:

                if (showingFirst == false) {
                    SORT_LIST("dateformat");
                    showingFirst = true;
                } else {

                    DES_LIST("dateformat");

                    showingFirst = false;
                }


                break;
        }

    }

    private void DES_LIST(final String string) {

        Collections.sort(list, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return secondValue.compareToIgnoreCase(firstValue);
            }
        });

        AdaptorFavourite adaptorFavourite = new AdaptorFavourite(getActivity(), list);
        favorite_lv.setAdapter(adaptorFavourite);

        if (string.equals("title")) {
            headerBelow_tv.setText(list.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(list.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(list.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }

    public void SORT_LIST(final String string) {

        Collections.sort(list, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return firstValue.compareToIgnoreCase(secondValue);
            }
        });

        AdaptorFavourite adaptorFavourite = new AdaptorFavourite(getActivity(), list);
        favorite_lv.setAdapter(adaptorFavourite);

        if (string.equals("title")) {
            headerBelow_tv.setText(list.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(list.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(list.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }
}
