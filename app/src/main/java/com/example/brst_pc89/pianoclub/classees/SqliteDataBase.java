package com.example.brst_pc89.pianoclub.classees;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.brst_pc89.pianoclub.adaptor.PopularAdaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class SqliteDataBase {


    public static SQLiteDatabase sqliteDataBase;
    public static List<HashMap> list = new ArrayList<>();
    public static List<HashMap> listvideo = new ArrayList<>();
    public static List<String> listId = new ArrayList<>();

    public static void INSERT_DATA(Context context, String videoId, String videoTitle, String videoArtist, String publishDate, String videoRuntime, String songkey, String instructor, String mp4, String youTube) {

        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);

        sqliteDataBase.execSQL(DataBase_Query.CREATE_TABLE);

        ContentValues contentValues = new ContentValues();
        contentValues.put("Video_Id", videoId);
        contentValues.put("Video_Title", videoTitle);
        contentValues.put("Video_Artist", videoArtist);
        contentValues.put("Publish_Date", publishDate);
        contentValues.put("Video_Runtime", videoRuntime);
        contentValues.put("Song_Key", songkey);
        contentValues.put("Instructor_Id", instructor);
        contentValues.put("Mp4_Name", mp4);
        contentValues.put("You_Tube", youTube);


        sqliteDataBase.insert("Api_Table", null, contentValues);


    }

    public static List<HashMap> SELECT_DATA(Context context) {
        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);
        sqliteDataBase.execSQL(DataBase_Query.CREATE_TABLE);
        Cursor cursor = sqliteDataBase.rawQuery(DataBase_Query.SELECT_DATA, null);

        list.clear();
        if(cursor!=null && cursor.getCount()>0) {
            cursor.moveToFirst();
            do {

                String Video_Id = cursor.getString(1);
                String Video_Title = cursor.getString(2);
                String Video_Artist = cursor.getString(3);
                String Publish_Date = cursor.getString(4);
                String Video_Runtime = cursor.getString(5);
                String Song_Key = cursor.getString(6);
                String Instructor_Id = cursor.getString(7);
                String Mp4_Name = cursor.getString(8);
                String You_Tube = cursor.getString(9);

                HashMap hashMap = new HashMap();
                hashMap.put(MyConstant.VIDEO_ID, Video_Id);
                hashMap.put(MyConstant.VIDEO_TITLE, Video_Title);
                hashMap.put(MyConstant.VIDEO_ARTIST, Video_Artist);
                hashMap.put(MyConstant.PUBLISH_DATE, Publish_Date);
                hashMap.put(MyConstant.VIDEO_RUNTIME, Video_Runtime);
                hashMap.put(MyConstant.SONG_KEY, Song_Key);
                hashMap.put(MyConstant.INSTRUCTOR_ID, Instructor_Id);
                hashMap.put(MyConstant.MP4_NAME, Mp4_Name);
                hashMap.put(MyConstant.YOU_TUBE, You_Tube);

                list.add(hashMap);

            } while (cursor.moveToNext());
        }

        return list;
    }


    public static  void DELETE_DATA(Context context, String video_id)
    {

        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);

        sqliteDataBase.delete("Api_Table","Video_Id=? ",new String[]{video_id});
    }

    public static List<String> SELECT_ID(Context context)
    {
        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);
        sqliteDataBase.execSQL(DataBase_Query.CREATE_TABLE);
        Cursor cursor = sqliteDataBase.rawQuery(DataBase_Query.SELECT_DATA, null);

        listId.clear();

        if(cursor!=null && cursor.getCount()>0) {
            cursor.moveToFirst();
            do {

                String Video_Id = cursor.getString(1);

                listId.add(Video_Id);

            } while (cursor.moveToNext());

        }

        return listId;

    }

    public static void INSERT_VIDEO(Context context, String videoId, String videoPath, String videoTitle, String artist, String date, String videoRuntime, String songKey_array, String instructor_array, String mp4Name, String youTube) {

        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);

        sqliteDataBase.execSQL(DataBase_Query.CREATE_VIDEO_TABLE);

        ContentValues contentValues = new ContentValues();
        contentValues.put("VideoID", videoId);
        contentValues.put("VideoPath", videoPath);
        contentValues.put("VideoTitle", videoTitle);
        contentValues.put("VideoArtist", artist);
        contentValues.put("PublishDate", date);
        contentValues.put("VideoRuntime", videoRuntime);
        contentValues.put("SongKey", songKey_array);
        contentValues.put("InstructorID", instructor_array);
        contentValues.put("MP4Name", mp4Name);
        contentValues.put("YouTubePreviewID", youTube);


        sqliteDataBase.insert("Video_Table", null, contentValues);


    }

    public static List<HashMap> SELECT_DOWNLOAD_VIDEO(Context context) {
        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);
        sqliteDataBase.execSQL(DataBase_Query.CREATE_VIDEO_TABLE);

        Cursor cursor = sqliteDataBase.rawQuery(DataBase_Query.SELECT_VIDEO, null);

        listvideo.clear();
        if(cursor!=null && cursor.getCount()>0) {
            cursor.moveToFirst();
            do {

                String Video_Id = cursor.getString(0);
                String Video_Path = cursor.getString(1);
                String Video_Title = cursor.getString(2);
                String Video_artist = cursor.getString(3);
                String Video_Date = cursor.getString(4);
                String Video_Runtime = cursor.getString(5);
                String Video_SongKey = cursor.getString(6);
                String Video_Instructor = cursor.getString(7);
                String Video_Mp4 = cursor.getString(8);
                String Video_YouTube = cursor.getString(9);


                HashMap hashMap = new HashMap();
                hashMap.put(MyConstant.VIDEO_ID, Video_Id);
                hashMap.put(MyConstant.VIDEO_PATH, Video_Path);
                hashMap.put(MyConstant.VIDEO_TITLE, Video_Title);
                hashMap.put(MyConstant.VIDEO_ARTIST, Video_artist);
                hashMap.put(MyConstant.PUBLISH_DATE, Video_Date);
                hashMap.put(MyConstant.VIDEO_RUNTIME, Video_Runtime);
                hashMap.put(MyConstant.SONG_KEY, Video_SongKey);
                hashMap.put(MyConstant.INSTRUCTOR_ID, Video_Instructor);
                hashMap.put(MyConstant.MP4_NAME, Video_Mp4);
                hashMap.put(MyConstant.YOU_TUBE, Video_YouTube);


                listvideo.add(hashMap);

            } while (cursor.moveToNext());
        }

        return listvideo;
    }

    public static  void DELETE_VIDEO(Context context, String video_id)
    {

        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);

        sqliteDataBase.delete("Video_Table","VideoID=? ",new String[]{video_id});
    }

    public void INSERT_KEY_LIST(Context context)
    {
        sqliteDataBase = context.openOrCreateDatabase(MyConstant.SAVEDATA_DATABASE, Context.MODE_PRIVATE, null);
    }

}
