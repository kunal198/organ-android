package com.example.brst_pc89.pianoclub.adaptor;

/**
 * Created by brst-pc89 on 3/31/17.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.DateFormat;
import com.example.brst_pc89.pianoclub.classees.DownloadTask;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SongKey;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;

import java.util.HashMap;
import java.util.List;





/**
 * Created by brst-pc89 on 3/22/17.
 */
public class SearchAdaptor extends BaseExpandableListAdapter {

    Context context;
    List<HashMap> responseList;
    List<HashMap> videoList;
    List<String> databaseList;
    LayoutInflater inflater;
    Dialog dialog;
    TextView logout_tv, logCancel_tv, log_tv;


    public SearchAdaptor(Context context, List<HashMap> responseList) {

        this.context = context;
        this.responseList = responseList;

        Log.d("sdds", responseList + "");
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        gET_LIST();

        GET_VIDEO_LIST();
    }

    private void GET_VIDEO_LIST() {

        videoList = SqliteDataBase.SELECT_DOWNLOAD_VIDEO(context);

        Log.d("sdfs", videoList + "");

    }

    private void gET_LIST() {

        databaseList = SqliteDataBase.SELECT_ID(context);
    }

    @Override
    public int getGroupCount() {
        return responseList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.custom_favourite_layout, null);
        TextView leftText_tv = (TextView) view.findViewById(R.id.leftText_tv);
        TextView centerText_tv = (TextView) view.findViewById(R.id.centerText_tv);

        HashMap hashMap = responseList.get(groupPosition);
        Object videoId = hashMap.get(MyConstant.VIDEO_ID);
        Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);


        leftText_tv.setText(videoTitle.toString());
        centerText_tv.setText("(" + videoArtist.toString() + ")");

        ImageView rowDown_iv = (ImageView) view.findViewById(R.id.rowDown_iv);


        if (isExpanded) {
            rowDown_iv.setImageResource(R.mipmap.ic_row_down);
            rowDown_iv.setRotation(360);
        }
        return view;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        View view = inflater.inflate(R.layout.custom_expandablelayout, null);
        TextView videoLength_tv = (TextView) view.findViewById(R.id.videoLength_tv);
        TextView instuctor_tv = (TextView) view.findViewById(R.id.instuctor_tv);
        TextView songKey_tv = (TextView) view.findViewById(R.id.songKey_tv);
        TextView date_tv = (TextView) view.findViewById(R.id.date_tv);
        final TextView download_tv = (TextView) view.findViewById(R.id.download_tv);
        ImageView expfavrt_iv = (ImageView) view.findViewById(R.id.expfavrt_iv);
        ImageView favrt_iv = (ImageView) view.findViewById(R.id.favrt_iv);
        final ImageView watch_iv = (ImageView) view.findViewById(R.id.watch_iv);
        final ImageView watchOffline_iv = (ImageView) view.findViewById(R.id.watchOffline_iv);
        final ImageView delete_iv = (ImageView) view.findViewById(R.id.delete_iv);
        final ImageView download_iv = (ImageView) view.findViewById(R.id.download_iv);


        HashMap hashMap = responseList.get(groupPosition);
        final Object videoId = hashMap.get(MyConstant.VIDEO_ID);
        final Object videoTitle = hashMap.get(MyConstant.VIDEO_TITLE);
        final Object videoArtist = hashMap.get(MyConstant.VIDEO_ARTIST);
        final Object publishDate = hashMap.get(MyConstant.PUBLISH_DATE);
        final Object videoRuntime = hashMap.get(MyConstant.VIDEO_RUNTIME);
        final Object songKey = hashMap.get(MyConstant.SONG_KEY);
        final Object instructorId = hashMap.get(MyConstant.INSTRUCTOR_ID);
        final Object mp4Name = hashMap.get(MyConstant.MP4_NAME);
        final Object youTube = hashMap.get(MyConstant.YOU_TUBE);

        if (databaseList.contains(videoId)) {

            favrt_iv.setVisibility(View.VISIBLE);
            expfavrt_iv.setVisibility(View.GONE);
            Log.e("videoId while", videoId + "");
        }

        for (int i = 0; i < videoList.size(); i++) {
            if (videoList.get(i).get(MyConstant.VIDEO_ID).equals(videoId)) {
                //  videoPath=videoList.get(i).get(MyConstant.VIDEO_PATH).toString();
                Log.d("dfd", "dsfs");
                download_iv.setVisibility(View.GONE);
                watchOffline_iv.setVisibility(View.VISIBLE);
                watch_iv.setVisibility(View.GONE);
                download_tv.setVisibility(View.VISIBLE);
                delete_iv.setVisibility(View.VISIBLE);
                break;
            }
        }

        watchOffline_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Path = GET_PATH(videoId.toString());
                Log.d("fd", Path);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Path));
                intent.setDataAndType(Uri.parse(Path), "video/mp4");
                context.startActivity(intent);
            }
        });

        final int songKey_val = Integer.parseInt(songKey.toString());
        final int instructor_val = Integer.parseInt(instructorId.toString());

        final String date = DateFormat.PARSE_DATE_FORMAT(publishDate.toString());

        videoLength_tv.setText(videoRuntime + " " + "min");

        songKey_tv.setText(SongKey.songKey_array[songKey_val]);
        date_tv.setText(date);
        instuctor_tv.setText(SongKey.instructor_array[instructor_val - 1]);

        final String videoPath = "https://s3.amazonaws.com/pchmp4/" + mp4Name;
        watch_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                intent.setDataAndType(Uri.parse(videoPath), "video/mp4");
                context.startActivity(intent);
            }
        });

        download_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProgressDialog mProgressDialog;
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage(videoTitle.toString());
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);


                final DownloadTask downloadTask = new DownloadTask(context,SearchAdaptor.this, mProgressDialog, videoTitle.toString(), videoId.toString(),videoArtist.toString(),
                        date, videoRuntime.toString(), SongKey.songKey_array[songKey_val],
                        SongKey.instructor_array[instructor_val - 1], mp4Name.toString(), youTube.toString());

                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        downloadTask.cancel(true);
                    }
                });

                downloadTask.execute("https://s3.amazonaws.com/pchmp4/" + mp4Name);

                mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        downloadTask.cancel(true);
                    }
                });

            }
        });

        expfavrt_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SqliteDataBase.INSERT_DATA(context, videoId.toString(), videoTitle.toString(), videoArtist.toString(),
                        date, videoRuntime.toString(), SongKey.songKey_array[songKey_val],
                        SongKey.instructor_array[instructor_val - 1], mp4Name.toString(), youTube.toString());

                Log.e("videoId", videoId + "------------" + videoTitle);

                gET_LIST();
                notifyDataSetChanged();


            }
        });

        favrt_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DELETE_DIALOG(context, videoId,false);


            }
        });

        delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DELETE_DIALOG(context, videoId, true);


            }
        });

        return view;
    }


    public void refreshMe()
    {
        GET_VIDEO_LIST();
        notifyDataSetChanged();
    }

    private String GET_PATH(String videoId) {

        String videoPath = "";
        for (int i = 0; i < videoList.size(); i++) {
            if (videoList.get(i).get(MyConstant.VIDEO_ID).equals(videoId)) {
                videoPath = videoList.get(i).get(MyConstant.VIDEO_PATH).toString();
                break;
            }
        }
        return videoPath;

    }

    private void DELETE_DIALOG(final Context context, final Object videoId, final boolean b) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        logout_tv = (TextView) dialog.findViewById(R.id.logout_tv);
        logCancel_tv = (TextView) dialog.findViewById(R.id.logCancel_tv);
        log_tv = (TextView) dialog.findViewById(R.id.log_tv);
        if (b) {
            log_tv.setText("Confirm removal of this download?");
            logout_tv.setText("Ok");
            logCancel_tv.setText("Cancel");
        } else {
            log_tv.setText("Remove this favorite?");
        }


        logout_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!b) {

                    SqliteDataBase.DELETE_DATA(context, videoId.toString());

                    gET_LIST();
                    notifyDataSetChanged();
                }

                else
                {
                    SqliteDataBase.DELETE_VIDEO(context, videoId.toString());
                    GET_VIDEO_LIST();
                    notifyDataSetChanged();
                    CONFIRM_DIALOG(context);
                }
                dialog.dismiss();
            }
        });

        logCancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

    }
    private void CONFIRM_DIALOG(Context context) {

        final Dialog  dialogdel = new Dialog(context);
        dialogdel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogdel.setContentView(R.layout.dialog_filter);
        dialogdel.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView content_tv=(TextView)dialogdel.findViewById(R.id.content_tv);
        TextView dialogOk_tv=(TextView)dialogdel.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Successfully deleted.");
        dialogdel.show();

        dialogOk_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogdel.dismiss();
            }
        });
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);


    }

}

