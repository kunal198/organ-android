package com.example.brst_pc89.pianoclub.classees;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class DataBase_Query {


    public static final String CREATE_TABLE="create table if not exists Api_Table(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
            "Video_Id text,Video_Title text,Video_Artist text,Publish_Date text,Video_Runtime text," +
            "Song_Key text,Instructor_Id text,Mp4_Name text,You_Tube text)";

    public static final String SELECT_DATA="select * from Api_Table";

    public static final String DELETE_DATA="delete * from Api_Table";

    public static final String CREATE_VIDEO_TABLE="create table if not exists Video_Table(VideoID text,VideoPath text,VideoTitle text,VideoArtist text,PublishDate text,VideoRuntime text,SongKey text,InstructorID text,MP4Name text,YouTubePreviewID text)";

    public static final String SELECT_VIDEO="select * from Video_Table";

    public static final String CREATE_KEY_TABLE="create table if not exists Key_Table(keyText text,keyValue text)";

}
