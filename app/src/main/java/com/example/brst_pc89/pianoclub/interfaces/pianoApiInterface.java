package com.example.brst_pc89.pianoclub.interfaces;


import com.example.brst_pc89.pianoclub.classees.GetCategory;
import com.example.brst_pc89.pianoclub.classees.GetVersion;
import com.example.brst_pc89.pianoclub.classees.RequestClass;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by brst-pc89 on 3/24/17.
 */
public interface pianoApiInterface {

    @GET("api/login.php")
    Call<String> logincall(@Query("name") String username, @Query("pwd") String passsword);

    @GET("api/?option=get_video")
    Call<List<HashMap>> getJson(@Query("category") int value);

    @POST("api/search.php")
    @FormUrlEncoded
    Call<List<HashMap>> getJsonResponse(@Field("data") String jsonString);


    @GET("api/?option=get_version")
    Call<GetVersion> version();


    @GET("api/?option=get_category_list")
    Call<List<HashMap>> category();

    @GET("api/?option=get_video_request")
    Call<RequestClass> submitRequest(@Query("category") int category,
                                     @Query("videoname") String songname,
                                     @Query("user_id") String username,
                                     @Query("artist") String artist

    );

  @GET("api/?option=get_video&category=0&standardview=1")
    Call<List<HashMap>> GuestVideo_List();

}
