package com.example.brst_pc89.pianoclub.classees;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by brst-pc89 on 3/27/17.
 */
public class ApiClient {
    //http://www.organclubhouse.com/api
    public static final String BASE_URL =  "http://organclubhouse.com/";

    private static Retrofit retrofit = null;


    public static Retrofit getClient() {

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
