package com.example.brst_pc89.pianoclub.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class BrowseLoginActivity extends AppCompatActivity implements View.OnClickListener{

    TextView login_tv, browse_tv;
    RewardedVideoAd rewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_login);

        SharedPreference.getInstance().storeData(getApplication(),SharedPreference.BOOLEAN,"false");

        SETUP_VIEWS();

        SET_LISTENER();

    }

    private void SET_LISTENER() {

        login_tv.setOnClickListener(this);
        browse_tv.setOnClickListener(this);
    }

    private void SETUP_VIEWS() {

        login_tv = (TextView) findViewById(R.id.login_tv);
        browse_tv = (TextView) findViewById(R.id.browse_tv);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.login_tv:

                Intent intent = new Intent(BrowseLoginActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

                break;


            case R.id.browse_tv:

                Intent browseIntent = new Intent(BrowseLoginActivity.this, BrowseVideoActivity.class);
                startActivity(browseIntent);
                finish();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
    }



}
