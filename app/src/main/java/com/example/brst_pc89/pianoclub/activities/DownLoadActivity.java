package com.example.brst_pc89.pianoclub.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.adaptor.DownloadAct_Adapter;
import com.example.brst_pc89.pianoclub.adaptor.DownloadAdapter;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SqliteDataBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class DownLoadActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView popUpdismiss_iv, popUp_iv,back_iv;
    ExpandableListView downloadFragment_lv;
    List<HashMap> databaseList;
    List<String> list;
    DownLoadActivity downLoadActivity;
    TextView headerAbove_tv, headerBelow_tv;
    public static PopupWindow popupWindow;
    boolean showingFirst = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_down_load);
        downLoadActivity = this;
        GET_LAYOUT_ID();
        SELECT_DATA();
    }

    public void SELECT_DATA() {


        databaseList = SqliteDataBase.SELECT_DOWNLOAD_VIDEO(getBaseContext());

        Log.d("list", list + "");
        Log.d("data", databaseList + "");
        headerBelow_tv.setVisibility(View.VISIBLE);
        headerBelow_tv.setText(list.size() + " " + "videos, sorted by publish date");

        DownloadAct_Adapter downloadAdapter = new DownloadAct_Adapter(this, databaseList, downLoadActivity);
        downloadFragment_lv.setAdapter(downloadAdapter);


    }

    private void GET_LAYOUT_ID() {

        popUp_iv = (ImageView) findViewById(R.id.popUp_iv);
        headerAbove_tv = (TextView) findViewById(R.id.headerAbove_tv);
        headerBelow_tv = (TextView) findViewById(R.id.headerBelow_tv);
        popUpdismiss_iv = (ImageView) findViewById(R.id.popUpdismiss_iv);
        back_iv = (ImageView) findViewById(R.id.back_iv);
        downloadFragment_lv = (ExpandableListView) findViewById(R.id.downloadFragment_lv);
        list = new ArrayList<String>();
        databaseList = new ArrayList<HashMap>();
        popUp_iv.setOnClickListener(this);
        back_iv.setOnClickListener(this);

        headerAbove_tv.setText("DownLoads");

    }

    public void Referesh_me() {

        SELECT_DATA();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.back_iv:
            {
               finish();
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
                break;
            }

            case R.id.popUp_iv: {
                showPopup(popUp_iv);
                break;
            }

            case R.id.title_tv:

                if (showingFirst == false) {
                    SORT_LIST("title");
                    showingFirst = true;
                } else {

                    DES_LIST("title");

                    showingFirst = false;
                }
                break;

            case R.id.artist_tv:


                if (showingFirst == false) {
                    SORT_LIST("artist");
                    showingFirst = true;
                } else {

                    DES_LIST("artist");

                    showingFirst = false;
                }

                break;

            case R.id.date_tv:

                if (showingFirst == false) {
                    SORT_LIST("dateformat");
                    showingFirst = true;
                } else {

                    DES_LIST("dateformat");

                    showingFirst = false;
                }


                break;

        }

    }

    private void showPopup(final ImageView popUp_iv) {

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_popup, null);
        TextView title_tv = (TextView) popupView.findViewById(R.id.title_tv);
        TextView artist_tv = (TextView) popupView.findViewById(R.id.artist_tv);
        TextView date_tv = (TextView) popupView.findViewById(R.id.date_tv);

        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //TODO do sth here on dismiss
                popupWindow.dismiss();
            }
        });

        popupWindow.showAsDropDown(popUp_iv, 0, -40);

        popUp_iv.setVisibility(View.GONE);
        popUpdismiss_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                popUp_iv.setVisibility(View.VISIBLE);
                popUpdismiss_iv.setVisibility(View.GONE);

            }
        });

        title_tv.setOnClickListener(this);
        artist_tv.setOnClickListener(this);
        date_tv.setOnClickListener(this);
    }


    private void DES_LIST(final String string) {

        Collections.sort(databaseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return secondValue.compareToIgnoreCase(firstValue);
            }
        });

        DownloadAct_Adapter downloadAdapter = new DownloadAct_Adapter(getBaseContext(), databaseList, downLoadActivity);
        downloadFragment_lv.setAdapter(downloadAdapter);

        if (string.equals("title")) {
            headerBelow_tv.setText(databaseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(databaseList.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(databaseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }

    public void SORT_LIST(final String string) {

        Collections.sort(databaseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return firstValue.compareToIgnoreCase(secondValue);
            }
        });

        DownloadAct_Adapter downloadAdapter = new DownloadAct_Adapter(getBaseContext(), databaseList, downLoadActivity);
        downloadFragment_lv.setAdapter(downloadAdapter);

        if (string.equals("title")) {
            headerBelow_tv.setText(databaseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(databaseList.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(databaseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }
}
