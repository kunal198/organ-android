package com.example.brst_pc89.pianoclub.fragments;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    ImageView fragmentKey_iv, fragmentGenre_iv, fragmenttaught_iv, display_iv, reset_iv;
    EditText searh_et;
    TextView keyFilter_tv, genreFilter_tv, insFilter_tv;

    KeyFragment keyFragment = new KeyFragment();
    GenreFragment genreFragment = new GenreFragment();
    InstructuctorFragment instructuctorFragment = new InstructuctorFragment();
    SearchResultFragment searchResultFragment = new SearchResultFragment();
    List<String> KeyList = new ArrayList<>();
    List<String> GenreList = new ArrayList<>();
    List<String> InstructrList = new ArrayList<>();

    Bundle args;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        fragmentKey_iv = (ImageView) view.findViewById(R.id.fragmentKey_iv);
        fragmentGenre_iv = (ImageView) view.findViewById(R.id.fragmentGenre_iv);
        fragmenttaught_iv = (ImageView) view.findViewById(R.id.fragmenttaught_iv);
        display_iv = (ImageView) view.findViewById(R.id.display_iv);
        reset_iv = (ImageView) view.findViewById(R.id.reset_iv);
        searh_et = (EditText) view.findViewById(R.id.searh_et);
        keyFilter_tv = (TextView) view.findViewById(R.id.keyFilter_tv);
        genreFilter_tv = (TextView) view.findViewById(R.id.genreFilter_tv);
        insFilter_tv = (TextView) view.findViewById(R.id.insFiler_tv);
        KeyList = SharedPreference.getInstance().geKeyList(getContext(), SharedPreference.KEY_LIST);
        GenreList = SharedPreference.getInstance().geGenreList(getContext(), SharedPreference.GENRE_LIST);
        InstructrList = SharedPreference.getInstance().geInstructorList(getContext(), SharedPreference.INSTRUCTOR_LIST);
        Log.d("keyList", KeyList+ "................" + GenreList + "..............." + InstructrList);


        Log.d("keydfdsList", KeyList.size() + "................" + GenreList + "..............." + InstructrList);

        SET_KEYFILTER_TEXT();

        SET_GENREFILTER_TEXT();

        SET_INSFILTER_TEXT();




        display_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String text = searh_et.getText().toString();
                try {
                    JSONObject object = new JSONObject("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("keyword", text);
                    JSONArray key_list = new JSONArray(KeyList);
                    jsonObject.put("key", key_list);
                    JSONArray genre_list = new JSONArray(GenreList);
                    jsonObject.put("genre", genre_list);
                    JSONArray ins_list = new JSONArray(InstructrList);
                    jsonObject.put("instructor", ins_list);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (searh_et.getText().toString().isEmpty() && KeyList.size() == 0 && GenreList.size() == 0 && InstructrList.size() == 0) {
                    openDialog(getContext());
                } else

                {
                    args = new Bundle();
                    String jsonString = jsonObject.toString();
                    args.putString("stringdata", jsonString);
                    replaceFragment(searchResultFragment);
                }

            }
        });


   searh_et.addTextChangedListener(new TextWatcher() {
       @Override
       public void beforeTextChanged(CharSequence s, int start, int count, int after) {



       }

       @Override
       public void onTextChanged(CharSequence s, int start, int before, int count) {

           if ( KeyList.size()>0 || GenreList.size()>0 || InstructrList.size()>0 || s.length()>0 )
           {

               reset_iv.setEnabled(true);
           }
           else
           {

               reset_iv.setEnabled(false);
           }

       }

       @Override
       public void afterTextChanged(Editable s) {

       }
   });

            reset_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   Log.d("sc", String.valueOf(searh_et.getText().toString().trim().length())) ;
                    searh_et.setText("");
                    SharedPreference.getInstance().clearDataList(getContext(), SharedPreference.KEY_LIST);
                    SharedPreference.getInstance().clearDataList(getContext(), SharedPreference.GENRE_LIST);
                    SharedPreference.getInstance().clearDataList(getContext(), SharedPreference.INSTRUCTOR_LIST);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.detach(SearchFragment.this).attach(SearchFragment.this).commit();


                }
            });


        fragmentKey_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replaceFragment(keyFragment);
            }
        });

        fragmentGenre_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replaceFragment(genreFragment);
            }
        });

        fragmenttaught_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(instructuctorFragment);
            }
        });

        return view;
    }

    private void SET_INSFILTER_TEXT() {


        if (InstructrList.contains(String.valueOf(0))) {
            if (InstructrList.size() > 1) {

                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");
                    insFilter_tv.append("Tj Hanes");

                } else {
                    insFilter_tv.setTextColor(R.color.colorGrays);

                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Tj Hanes");
            }
        }

        if (InstructrList.contains(String.valueOf(1))) {
            if (InstructrList.size() > 1) {

                  insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");
                    insFilter_tv.append("Carlton Whitfield");

                } else {

                    insFilter_tv.append(", Carlton Whitfield");
                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Carlton Whitfield");
            }
        }

        if (InstructrList.contains(String.valueOf(2))) {
            if (InstructrList.size() > 1) {

                 insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");
                    insFilter_tv.append("Darrell Cook");

                } else {

                    insFilter_tv.append(", Darrell Cook");
                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Darrell Cook");
            }
        }

        if (InstructrList.contains(String.valueOf(3))) {
            if (InstructrList.size() > 1) {

                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");
                    insFilter_tv.append("Shaun Martin");

                } else {

                    insFilter_tv.append(", Shaun Martin");
                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Shaun Martin");
            }
        }

        if (InstructrList.contains(String.valueOf(4))) {
            if (InstructrList.size() > 1) {
                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");

                    insFilter_tv.append("Rick Barclay");
                } else {

                    insFilter_tv.append(", Rick Barclay");
                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Rick Barclay");
            }
        }

        if (InstructrList.contains(String.valueOf(5))) {
            if (InstructrList.size() > 1) {

                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");

                    insFilter_tv.append("David Papangis");
                } else {

                    insFilter_tv.append(", David Papangis");
                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("David Papangis");
            }
        }

        if (InstructrList.contains(String.valueOf(6))) {
            if (InstructrList.size() > 1) {
                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");
                    insFilter_tv.append("Gogo");

                } else {

                    insFilter_tv.append(", Gogo");
                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Gogo");
            }
        }

        if (InstructrList.contains(String.valueOf(7))) {
            if (InstructrList.size() > 1) {
                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");
                    insFilter_tv.append("Peter Thompson");

                } else {

                    insFilter_tv.append(", Peter Thompson");
                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Peter Thompson");
            }
        }


        if (InstructrList.contains(String.valueOf(8))) {
            if (InstructrList.size() > 1) {
                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");

                    insFilter_tv.append("Stephano Buchanan");
                } else {
                    insFilter_tv.append(", Stephano Buchanan");

                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Stephano Buchanan");
            }
        }


        if (InstructrList.contains(String.valueOf(9))) {
            if (InstructrList.size() > 1) {
                insFilter_tv.setTextColor(Color.GRAY);

                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");

                    insFilter_tv.append("Anthony Brice");
                } else {
                    insFilter_tv.append(", Anthony Brice");

                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Anthony Brice");
            }
        }

        if (InstructrList.contains(String.valueOf(10))) {
            if (InstructrList.size() > 1) {
                insFilter_tv.setTextColor(Color.GRAY);
                if (insFilter_tv.getText().equals("no filters added yet...")) {
                    insFilter_tv.setText("");

                    insFilter_tv.append("Jermaine Roberts");
                } else {
                    insFilter_tv.append(", Jermaine Roberts");

                }
            } else {
                insFilter_tv.setTextColor(Color.GRAY);
                insFilter_tv.setText("Jermaine Roberts");
            }
        }


    }

    private void SET_GENREFILTER_TEXT() {

        if (GenreList.contains(String.valueOf(8))) {
            if (GenreList.size() > 1) {

                  genreFilter_tv.setTextColor(Color.GRAY);

                if (genreFilter_tv.getText().equals("no filters added yet...")) {
                    genreFilter_tv.setText("");
                    genreFilter_tv.append("Chords & Progession");

                } else {
                    genreFilter_tv.append(", Chords & Progession");

                }
            } else {
                genreFilter_tv.setTextColor(Color.GRAY);
                genreFilter_tv.setText("Chords & Progession");
            }
        }

        if (GenreList.contains(String.valueOf(6))) {
            if (GenreList.size() > 1) {
                genreFilter_tv.setTextColor(Color.GRAY);

                if (genreFilter_tv.getText().equals("no filters added yet...")) {
                    genreFilter_tv.setText("");
                    genreFilter_tv.append("Other");

                } else {
                    genreFilter_tv.append(", Other");
                }
            } else {
                genreFilter_tv.setTextColor(Color.GRAY);
                genreFilter_tv.setText("Other");
            }
        }

        if (GenreList.contains(String.valueOf(2))) {
            if (GenreList.size() > 1) {

                genreFilter_tv.setTextColor(Color.GRAY);

                if (genreFilter_tv.getText().equals("no filters added yet...")) {
                    genreFilter_tv.setText("");
                    genreFilter_tv.append("Preacher Chords");

                } else {

                    genreFilter_tv.append(", Preacher Chords");

                }
            } else {

                genreFilter_tv.setTextColor(Color.GRAY);

                genreFilter_tv.setText("Preacher Chords");
            }
        }

        if (GenreList.contains(String.valueOf(3))) {
            if (GenreList.size() > 1) {

                genreFilter_tv.setTextColor(Color.GRAY);

                if (genreFilter_tv.getText().equals("no filters added yet...")) {
                    genreFilter_tv.setText("");
                    genreFilter_tv.append("Runs");
                    genreFilter_tv.setTextColor(R.color.colorGray);
                }
         else
                    genreFilter_tv.append(", Runs");

            } else {
                genreFilter_tv.setTextColor(Color.GRAY);
                genreFilter_tv.setText("Runs");
            }
        }

        if (GenreList.contains(String.valueOf(5))) {
            if (GenreList.size() > 1) {
                genreFilter_tv.setTextColor(Color.GRAY);

                if (genreFilter_tv.getText().equals("no filters added yet...")) {
                    genreFilter_tv.setText("");
                    genreFilter_tv.append("Short Music");

                } else {
                    genreFilter_tv.append(", Short Music");

                }
            } else {
                genreFilter_tv.setTextColor(Color.GRAY);
                genreFilter_tv.setText("Short Music");
            }
        }

        if (GenreList.contains(String.valueOf(1))) {
            if (GenreList.size() > 1) {

                genreFilter_tv.setTextColor(Color.GRAY);
                if (genreFilter_tv.getText().equals("no filters added yet...")) {
                    genreFilter_tv.setText("");

                    genreFilter_tv.append("Song Tutorials");
                } else {
                    genreFilter_tv.append(", Song Tutorials");

                }
            } else {
                genreFilter_tv.setTextColor(Color.GRAY);
                genreFilter_tv.setText("Song Tutorials");
            }
        }

        if (GenreList.contains(String.valueOf(4))) {
            if (GenreList.size() > 1) {
                genreFilter_tv.setTextColor(Color.GRAY);

                if (genreFilter_tv.getText().equals("no filters added yet...")) {
                    genreFilter_tv.setText("");
                    genreFilter_tv.append("Talk & Prayer Music");
                    genreFilter_tv.setTextColor(Color.GRAY);
                } else {
                    genreFilter_tv.append(", Talk & Prayer Music");

                }
            } else {
                genreFilter_tv.setTextColor(Color.GRAY);
                genreFilter_tv.setText("Talk & Prayer Music");
            }
        }
    }

    private void SET_KEYFILTER_TEXT() {

        if (KeyList.contains(String.valueOf(10))) {
            if (KeyList.size() > 1) {

                   keyFilter_tv.setTextColor(Color.GRAY);

                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("A");

                } else {
                    keyFilter_tv.append(", A");

                }
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("A");
            }
        }

        if (KeyList.contains(String.valueOf(4))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);

                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");

                    keyFilter_tv.append("D#/Eb");
                } else {
                    keyFilter_tv.append(", D#/Eb");

                }
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("D#/Eb");
            }
        }

        if (KeyList.contains(String.valueOf(11))) {
            if (KeyList.size() > 1) {

                keyFilter_tv.setTextColor(Color.GRAY);

                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("A#Bb");

                } else {
                    keyFilter_tv.append(", A#Bb");

                }
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("A#Bb");
            }
        }

        if (KeyList.contains(String.valueOf(5))) {
            if (KeyList.size() > 1) {

                keyFilter_tv.setTextColor(Color.GRAY);

                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("E");

                } else {

                    keyFilter_tv.append(", E");

                }
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("E");
            }
        }

        if (KeyList.contains(String.valueOf(12))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);
                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("B");
                } else

                    keyFilter_tv.append(", B");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("B");
            }
        }

        if (KeyList.contains(String.valueOf(6))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);

                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("F");
                } else
                    keyFilter_tv.append(", F");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("F");
            }
        }

        if (KeyList.contains(String.valueOf(1))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);

                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("C");
                } else
                    keyFilter_tv.append(", C");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("C");
            }
        }

        if (KeyList.contains(String.valueOf(7))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);
                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("F#/Gb");
                } else
                    keyFilter_tv.append(", F#/Gb");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("F#/Gb");
            }
        }

        if (KeyList.contains(String.valueOf(2))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);
                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("C#Db");
                } else

                    keyFilter_tv.append(", C#Db");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("C#Db");
            }
        }

        if (KeyList.contains(String.valueOf(8))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);
                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("G");
                } else

                    keyFilter_tv.append(", G");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("G");
            }
        }

        if (KeyList.contains(String.valueOf(3))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);
                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("D");
                } else
                    keyFilter_tv.append(", D");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("D");
            }
        }

        if (KeyList.contains(String.valueOf(9))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);
                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("G#/Ab");
                } else
                    keyFilter_tv.append(", G#/Ab");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("G#/Ab");
            }
        }

        if (KeyList.contains(String.valueOf(0))) {
            if (KeyList.size() > 1) {
                keyFilter_tv.setTextColor(Color.GRAY);
                if (keyFilter_tv.getText().equals("no filters added yet...")) {
                    keyFilter_tv.setText("");
                    keyFilter_tv.append("no key");
                } else

                    keyFilter_tv.append(", no key");
            } else {
                keyFilter_tv.setTextColor(Color.GRAY);
                keyFilter_tv.setText("no key");
            }
        }
    }

    private void openDialog(Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_filter);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        TextView dialogOk_tv = (TextView) dialog.findViewById(R.id.dialogOk_tv);
        dialogOk_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void replaceFragment(Fragment fragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if(fragment==keyFragment) {
            transaction.replace(R.id.container, fragment, "KEY_FRAGMENT");
        }
        else if(fragment==genreFragment)
        {
            transaction.replace(R.id.container, fragment, "GENRE_FRAGMENT");
        }
        else if(fragment==instructuctorFragment)
        {
            transaction.replace(R.id.container, fragment, "INSTRUCTOR_FRAGMENT");
        }
       else if (fragment == searchResultFragment) {
            transaction.replace(R.id.container, fragment);
            fragment.setArguments(args);
        }
        transaction.addToBackStack(null);
        transaction.commit();


    }

}
