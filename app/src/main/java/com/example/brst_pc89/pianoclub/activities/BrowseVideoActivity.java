package com.example.brst_pc89.pianoclub.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.brst_pc89.pianoclub.R;
import com.example.brst_pc89.pianoclub.adaptor.BrowseVideoAdaptor;
import com.example.brst_pc89.pianoclub.adaptor.BrowserAdaptor;
import com.example.brst_pc89.pianoclub.classees.ApiClient;
import com.example.brst_pc89.pianoclub.classees.MyConstant;
import com.example.brst_pc89.pianoclub.classees.SharedPreference;
import com.example.brst_pc89.pianoclub.interfaces.pianoApiInterface;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrowseVideoActivity extends AppCompatActivity implements View.OnClickListener {


    ExpandableListView browseVideo_lv;
    TextView headerAbove_tv, headerBelow_tv;
    ImageView back_iv, popUp_iv, popUpdismiss_iv,banner_iv;
    PopupWindow popupWindow;
    boolean showingFirst = false;
    Dialog dialognet;
    ProgressDialog progressDialog;
    List<HashMap> responseList;

    InterstitialAd interstitialAd;



    AdRequest adRequest;

    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_video);

        String val=SharedPreference.getInstance().getData(getApplication(),SharedPreference.BOOLEAN);
        Log.d("sfd",val);

        if (TextUtils.isEmpty(getString(R.string.interstitial_full_screen))) {
            Toast.makeText(getApplicationContext(), "Please mention your Interstitial Ad ID in strings.xml", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(getString(R.string.banner_home_footer))) {
            Toast.makeText(getApplicationContext(), "Please mention your Banner Ad ID in strings.xml", Toast.LENGTH_LONG).show();
            return;
        }
         adRequest = new AdRequest.Builder()
                 .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("C04B1BFFB0774708339BC273F8A43708")
                .build();


        if(val.equals("false")) {

            INTERSITIAL();
        }

        SHOW_PROGRESS();

        SETUP_VIEWS();

        BANNER_ADS();

        SET_LISTENER();


        DATA_FROM_SERVER();
    }

    public void INTERSITIAL() {

        interstitialAd = new InterstitialAd(this);

        //set ad unit id

        interstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {


            public void onAdLoaded() {


                SHOW_ADS();

            }

            @Override
            public void onAdClosed() {


            }

            @Override
            public void onAdFailedToLoad(int errorCode) {


            }

            @Override
            public void onAdLeftApplication() {


            }


            @Override
            public void onAdOpened() {
                super.onAdOpened();


            }
        });

    }

    private void BANNER_ADS() {

        adView.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);

                Log.d("error",errorCode+"");
            }
        });

        adView.loadAd(adRequest);
    }

    private void SHOW_ADS() {


        if (interstitialAd.isLoaded()) {

            interstitialAd.show();
        }
    }


    private void SHOW_PROGRESS() {


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void DATA_FROM_SERVER() {

        pianoApiInterface apiInterface = ApiClient.getClient().create(pianoApiInterface.class);
        Call<List<HashMap>> call = apiInterface.GuestVideo_List();
        call.enqueue(new Callback<List<HashMap>>() {
            @Override
            public void onResponse(Call<List<HashMap>> call, Response<List<HashMap>> response) {


                responseList = new ArrayList<HashMap>();

                responseList = response.body();


                headerBelow_tv.setVisibility(View.VISIBLE);
                headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by publish date");

                BrowseVideoAdaptor browseVideoAdaptor = new BrowseVideoAdaptor(BrowseVideoActivity.this, responseList);
                browseVideo_lv.setAdapter(browseVideoAdaptor);
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<HashMap>> call, Throwable t) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        NETWORK_DIALOG(BrowseVideoActivity.this);
                        progressDialog.dismiss();

                    }
                }, 2000);


            }
        });

    }

    private void NETWORK_DIALOG(BrowseVideoActivity browseVideoActivity) {

        dialognet = new Dialog(browseVideoActivity);
        dialognet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialognet.setContentView(R.layout.dialog_filter);
        dialognet.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //   dialognet.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView content_tv = (TextView) dialognet.findViewById(R.id.content_tv);
        TextView dialogOk_tv = (TextView) dialognet.findViewById(R.id.dialogOk_tv);
        content_tv.setText("Connection or Download failed.");

        dialogOk_tv.setOnClickListener(this);
        dialognet.show();
    }

    private void SET_LISTENER() {

        back_iv.setOnClickListener(this);
        popUp_iv.setOnClickListener(this);
        banner_iv.setOnClickListener(this);
    }

    private void SETUP_VIEWS() {

        browseVideo_lv = (ExpandableListView) findViewById(R.id.browseVideo_lv);

        headerAbove_tv = (TextView) findViewById(R.id.headerAbove_tv);

        headerBelow_tv = (TextView) findViewById(R.id.headerBelow_tv);

        back_iv = (ImageView) findViewById(R.id.back_iv);

        popUp_iv = (ImageView) findViewById(R.id.popUp_iv);

        banner_iv = (ImageView) findViewById(R.id.banner_iv);

        popUpdismiss_iv = (ImageView) findViewById(R.id.popUpdismiss_iv);

        adView=(AdView)findViewById(R.id.adView);

        headerAbove_tv.setText("Free Tutorials");

        Picasso.with(this).load("http://www.organclubhouse.com/images/bannermobile/active.jpg").into(banner_iv);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_iv:
                Intent intent = new Intent(getApplication(), BrowseLoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                break;

            case R.id.popUp_iv:

                SHOW_POPUP(popUp_iv);
                break;

            case R.id.title_tv:

                if (showingFirst == false) {
                    SORT_LIST("title");
                    showingFirst = true;
                } else {

                    DES_LIST("title");

                    showingFirst = false;
                }
                break;

            case R.id.artist_tv:


                if (showingFirst == false) {

                    SORT_LIST("artist");

                    showingFirst = true;
                } else {

                    DES_LIST("artist");

                    showingFirst = false;
                }

                break;

            case R.id.date_tv:

                if (showingFirst == false) {
                    SORT_LIST("dateformat");
                    showingFirst = true;
                } else {

                    DES_LIST("dateformat");

                    showingFirst = false;
                }

                break;

            case R.id.dialogOk_tv:
                dialognet.dismiss();
                break;

            case R.id.banner_iv:

                //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.pianoclubhouse.com/w/membership-benefits/"));
                //startActivity(browserIntent);

                Intent intentBanner = new Intent(BrowseVideoActivity.this, BannerActivity.class);
                startActivity(intentBanner);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
               // finish();

              /*  Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.android.vending");
                startActivity(launchIntent);*/
        }
    }

    private void SHOW_POPUP(ImageView popUp_iv) {


        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_popup, null);

        TextView title_tv = (TextView) popupView.findViewById(R.id.title_tv);
        TextView artist_tv = (TextView) popupView.findViewById(R.id.artist_tv);
        TextView date_tv = (TextView) popupView.findViewById(R.id.date_tv);

        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);

        popupWindow.setFocusable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //TODO do sth here on dismiss

                popupWindow.dismiss();
            }
        });

        popupWindow.showAsDropDown(popUp_iv, 0, -40);

        this.popUp_iv.setVisibility(View.GONE);
        popUpdismiss_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                BrowseVideoActivity.this.popUp_iv.setVisibility(View.VISIBLE);
                popUpdismiss_iv.setVisibility(View.GONE);

            }
        });

        title_tv.setOnClickListener(this);
        artist_tv.setOnClickListener(this);
        date_tv.setOnClickListener(this);
    }


    private void
    DES_LIST(final String string) {

        Collections.sort(responseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return secondValue.compareToIgnoreCase(firstValue);
            }
        });
        BrowseVideoAdaptor browseVideoAdaptor = new BrowseVideoAdaptor(this, responseList);
        browseVideo_lv.setAdapter(browseVideoAdaptor);
        if (string.equals("title")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }


    public void SORT_LIST(final String string) {


        Collections.sort(responseList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (string.equals("title")) {

                    Log.d("sdcs", "title");

                    firstValue = s1.get(MyConstant.VIDEO_TITLE).toString();
                    secondValue = s2.get(MyConstant.VIDEO_TITLE).toString();
                } else if (string.equals("artist")) {
                    Log.d("sdcs", "artist");
                    firstValue = s1.get(MyConstant.VIDEO_ARTIST).toString();
                    secondValue = s2.get(MyConstant.VIDEO_ARTIST).toString();
                } else {
                    firstValue = s1.get(MyConstant.PUBLISH_DATE).toString();
                    secondValue = s2.get(MyConstant.PUBLISH_DATE).toString();
                }
                return firstValue.compareToIgnoreCase(secondValue);
            }
        });
        BrowseVideoAdaptor browseVideoAdaptor = new BrowseVideoAdaptor(this, responseList);
        browseVideo_lv.setAdapter(browseVideoAdaptor);
        if (string.equals("title")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by title");
        } else if (string.equals("artist")) {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by artist");
        } else {
            headerBelow_tv.setText(responseList.size() + " " + "videos, sorted by publish date");
        }
        popupWindow.dismiss();
        popUp_iv.setVisibility(View.VISIBLE);
        popUpdismiss_iv.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplication(), BrowseLoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        String videoId = SharedPreference.getInstance().getData(getApplication(), SharedPreference.VIDEO_ID);
        int  count = SharedPreference.getInstance().getCount(getApplication(), SharedPreference.VIDEO_COUNT);
        List<String> videoList = SharedPreference.getInstance().getVideoList(getApplication(), SharedPreference.VIDEO_LIST);

        if (requestCode == 100) {

          //  if (!videoList.contains(videoId)) {

              //  videoList.add(videoId);
              //  SharedPreference.getInstance().storeArrayList(getApplication(), SharedPreference.VIDEO_LIST, videoList);
                SharedPreference.getInstance().storeCount(getApplication(), SharedPreference.VIDEO_FLAG, 1);
                SharedPreference.getInstance().storeCount(getApplication(), SharedPreference.VIDEO_COUNT, count+1);

          //  }




        }
    }

    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }
}
